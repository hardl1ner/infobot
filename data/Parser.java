import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

public class Parser {
    private String username;
    private String password;
    private Properties props;
    public static void main(String[] args) {
        Parser parser = new Parser("it.spec2.ii@gmail.com", "it0501596170");
        String str = parser.parserWeatherDictionary();
        if(str != null) {
            System.out.println(str);
        }else{
            parser.send("InfoBot. Нет значения для погоды", "Добавте в словерь новое значение для проєкта " + ": "+ str, "it.spec2.ii@gmail.com", "hahyda14@gmail.com, hahyda15@outlook.com);
        }
    }

    public Parser(String username, String password) {
        this.username = username;
        this.password = password;

        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
    }

    public void send(String subject, String text, String fromEmail, String toEmail){
        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            Message message = new MimeMessage(session);
            //от кого
            message.setFrom(new InternetAddress(username));
            //кому
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            //Заголовок письма
            message.setSubject(subject);
            //Содержимое
            message.setText(text);

            //Отправляем сообщение
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }




    public String parserWeatherDictionary(String dictionaryFilePatch, String str) {
        String[] tmp = new String[2];
        String key;
        String value;
        String temp = null;
        HashMap<String, String> weatherDictionaryList = new HashMap<>();

        try {
            FileReader fr = new FileReader(dictionaryFilePatch);
            Scanner sc = new Scanner(fr);
            while (sc.hasNextLine()) {
                String s = sc.nextLine();
                tmp = s.split("=");
                key = tmp[0];
                value = tmp[1];
                weatherDictionaryList.put(key, value);
            }
            for (Map.Entry entry : weatherDictionaryList.entrySet()){
                if(entry.getKey().equals(str)){
                    str = entry.getValue().toString();
                }


            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return temp;


    }
}
