package main.java.yaroslav.khahyda;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class Test {
    public static void main(String[] args) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("http://api.airvisual.com/v2/nearest_city?key=b89d2b8e-44a4-4d74-a47a-feaad80fdbd6")
                .method("GET", null)
                .build();
        Response response = client.newCall(request).execute();
        String temp = response.body().string();
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(temp);
        System.out.println(temp);
        Integer i = JsonPath.read(document, "$.data.current.pollution.aqius");
        System.out.println(i);

    }
}
