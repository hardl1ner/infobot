package main.java.yaroslav.khahyda.address;

import main.java.yaroslav.khahyda.address.model.text.LayerImage;
import main.java.yaroslav.khahyda.address.model.text.LayerText;

public class UpdateWeather {
    private  String temperatureMax;
    private  String temperatureMin;
    private  String formatTemperature;
    private  String description;
    private boolean status = true;


    public UpdateWeather(String temperatureMax, String temperatureMin, String formatTemperature, String description) {
        this.temperatureMax = temperatureMax;
        this.temperatureMin = temperatureMin;
        this.formatTemperature = formatTemperature;
        this.description = description;
    }

    public Boolean setWeather(long idWeather, long idWeatherImg){
        UpdateLayerText updateLayerText = new UpdateLayerText();
        UpdateLayerImg updateLayerImg = new UpdateLayerImg();
        LayerText layerText = updateLayerText.getLayerText(idWeather);
        LayerImage layerImage = updateLayerImg.getLayerImg(idWeatherImg);

        if (layerText != null){
            if (layerImage != null){
                layerText.setText("<html>" + temperatureMax + formatTemperature + temperatureMin + "</html>");
                updateLayerText.updateText(layerText, idWeather);
                try {
                    long imageId = Long.parseLong(description);
                    layerImage.getFileInfo().setId(imageId);
                    updateLayerImg.updateImg(layerImage, idWeatherImg);
                } catch (NumberFormatException e) {
                    System.out.println("" + e);
                }

            } else {
                status = false;
            }
        } else {
            status = false;
        }

        return status;
}


}
