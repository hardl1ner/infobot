package main.java.yaroslav.khahyda.address;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import main.java.yaroslav.khahyda.address.model.text.LayerText;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UpdateCurrencyNBU {
    private  String USD;
    private  String EUR;
    private  String RUS;
    private String PLN;
    private  boolean status = true;

    public UpdateCurrencyNBU(String USD, String EUR, String RUS, String PLN) {
        this.USD = USD;
        this.EUR = EUR;
        this.RUS = RUS;
        this.PLN = PLN;
    }

    // изменяем значение существующего элемента name


    public Boolean setXmlCurrency(long idUSD, long idEUR, long idRUS, long idPLN){
        UpdateLayerText updateLayerText = new UpdateLayerText();
        Map<Long, String> currency = new HashMap<>();
        if (idUSD != 0) {
            currency.put(idUSD, USD);
        }
        if (idEUR != 0) {
            currency.put(idEUR, EUR);
        }
        if (idRUS != 0) {
            currency.put(idRUS, RUS);
        }
        if (idPLN != 0) {
            currency.put(idPLN, PLN);
        }
        if (currency.size() > 0) {
            currency.forEach((key, value) -> {
                LayerText layerText = updateLayerText.getLayerText(key);
                if (layerText != null) {
                    layerText.setText(value);
                    status = updateLayerText.updateText(layerText, key);
                } else {
                    status = false;
                }
            });
        } else {
            status = false;
        }

        return status;
    }
}
