package main.java.yaroslav.khahyda.address;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.java.yaroslav.khahyda.address.controller.WeatherController;
import main.java.yaroslav.khahyda.address.model.*;
import main.java.yaroslav.khahyda.address.model.text.Pollutant;
import main.java.yaroslav.khahyda.address.view.*;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class MainApp extends Application {
    public static Pattern pattern = Pattern.compile("\\d+\\.\\d{3}");// шаблон поиска значений курса НБУ
    private Stage primaryStage;
    private BorderPane rootLayout;
    public WebDriver driver;
    //контроллеры
    private BotOverviewController botOverviewController;
    private RootLayoutController rootLayoutController;
    private final ObservableList<TrafficApp> trafficAppData = FXCollections.observableArrayList();
    /**
     * Данные, в виде наблюдаемого списка погоды, пробок, курса
     */
    private final ObservableList<WeatherApp> weatherAppData = FXCollections.observableArrayList();
    private final ObservableList<CurrencyApp> currencyAppData = FXCollections.observableArrayList();
    private final ObservableList<IQAirApp> iqAirAppsData = FXCollections.observableArrayList();

    /**
     * Конструктор
     */
    public MainApp() {
        // В качестве образца добавляем некоторые данные
        weatherAppData.add(new WeatherApp("WOG", "WOG_Kyiv"));
        trafficAppData.add(new TrafficApp("OKKO", "OKKO_Kyiv"));
        currencyAppData.add(new CurrencyApp("WOG", "WOG"));
        iqAirAppsData.add(new IQAirApp("TRC", "Skymall"));


    }

    public static void main(String[] args) {
        launch(args);
    }

    // обработка полученной строки, и поиск нужных значений
    private static String[] getValueCurrency(String valueCurrency) {
        String[] valuesCurrencys = new String[3];
        Matcher matcher = pattern.matcher(valueCurrency);
        int count = 0;
        while (matcher.find()){ //если чтото нашел
            valuesCurrencys[count] = matcher.group(); //то записать значение
            count++;
            if (count == 3){ // после записи 3-х значений завершить работу
                break;
            }

        }
        return valuesCurrencys;
    }

    public BotOverviewController getBotOverviewController() {
        return botOverviewController;
    }

    /**
     * Возвращает данные в виде наблюдаемого списка погоды.
     */
    public ObservableList<WeatherApp> getWeatherAppData() {
        return weatherAppData;
    }

    /**
     * Возвращает данные в виде наблюдаемого списка пробок.
     */
    public ObservableList<TrafficApp> getTrafficAppData() {
        return trafficAppData;
    }
    public ObservableList<IQAirApp> getIqAirAppsData() {
        return iqAirAppsData;
    }

    /**
     * Инициализирует корневой макет и пытается загрузить последний открытый
     * файл.
     */
    public void initRootLayout() {
        try {
            // Загружаем корневой макет из fxml файла.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class
                    .getResource("/RootLayout.fxml"));
            rootLayout = loader.load();

            // Отображаем сцену, содержащую корневой макет.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);

            // Даём контроллеру доступ к главному приложению.
            rootLayoutController = loader.getController();
            rootLayoutController.setMainApp(this);


            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Пытается загрузить последний открытый файл.
        File file = getInfoBotFilePath();
        if (file != null) {
            loadInfoBotFromFile(file);
        }
    }

    /**
     * Показывает в корневом макете сведения об погоде, кусе пробках.
     */
    public void showBotOverview() {
        try {
            // Загружаем сведения об адресатах.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/BotOverview.fxml"));
            AnchorPane botOverview = loader.load();

            // Помещаем сведения об адресатах в центр корневого макета.
            rootLayout.setCenter(botOverview);

            // Даём контроллеру доступ к главному приложению.
            botOverviewController = loader.getController();
            botOverviewController.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Возвращает главную сцену.
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * Открывает диалоговое окно для изменения погоды.
     * Если пользователь кликнул OK, то изменения сохраняются в предоставленном
     * объекте погоды и возвращается значение true.
     *
     * @param weatherApp - объект адресата, который надо изменить
     * @return true, если пользователь кликнул OK, в противном случае false.
     */
    public boolean showWeatherAppEditDialog(WeatherApp weatherApp) {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/WeatherAppEditDialog.fxml"));
            AnchorPane page = loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Weather");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
            dialogStage.setResizable(false);
            // Устанавливаем иконку приложения.
            dialogStage.getIcons().add(new Image("file:resources/images/weatherApp.png"));

            // Передаём погоду в контроллер.
            WeatherAppEditDialogController weatherAppEditDialogController = loader.getController();
            weatherAppEditDialogController.setDialogWeatherStage(dialogStage);
            weatherAppEditDialogController.setWeatherApp(weatherApp);

            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();

            return weatherAppEditDialogController.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showIqAirAppEditDialog(IQAirApp iqAirApp) {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/IQAirAppEditDialog.fxml"));
            AnchorPane page = loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit IQAir");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
            dialogStage.setResizable(false);
            // Устанавливаем иконку приложения.
            //dialogStage.getIcons().add(new Image("file:resources/images/weatherApp.png"));

            // Передаём погоду в контроллер.
            IQAirAppEditDialogController iqAirAppEditDialogController = loader.getController();
            iqAirAppEditDialogController.setDialogIQAirStage(dialogStage);
            iqAirAppEditDialogController.setIQAirApp(iqAirApp);

            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();

            return iqAirAppEditDialogController.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Открывает диалоговое окно для изменения пробок.
     * Если пользователь кликнул OK, то изменения сохраняются в предоставленном
     * объекте пробок и возвращается значение true.
     *
     * @param trafficApp - объект пробок, который надо изменить
     * @return true, если пользователь кликнул OK, в противном случае false.
     */
    public boolean showTrafficAppEditDialog(TrafficApp trafficApp) {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/TrafficAppEditDialog.fxml"));
            AnchorPane page = loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogTrafficStage = new Stage();
            dialogTrafficStage.setTitle("Edit Traffic");
            dialogTrafficStage.initModality(Modality.WINDOW_MODAL);
            dialogTrafficStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogTrafficStage.setScene(scene);
            dialogTrafficStage.setResizable(false);
            // Устанавливаем иконку приложения.
            dialogTrafficStage.getIcons().add(new Image("file:resources/images/trafficApp.png"));

            // Передаём адресата в контроллер.
            TrafficAppEditDialogController trafficAppEditDialogController = loader.getController();
            trafficAppEditDialogController.setDialogTrafficStage(dialogTrafficStage);
            trafficAppEditDialogController.setTrafficApp(trafficApp);

            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogTrafficStage.showAndWait();

            return trafficAppEditDialogController.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Возвращает данные в виде наблюдаемого списка курса.
     */
    public ObservableList<CurrencyApp> getCurrencyAppData() {
        return currencyAppData;
    }

    /**
     * Возвращает preference файла бота, то есть, последний открытый файл.
     * Этот preference считывается из реестра, специфичного для конкретной
     * операционной системы. Если preference не был найден, то возвращается null.
     *
     */
    public File getInfoBotFilePath() {
        Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
        String filePath = prefs.get("filePath", null);
        if (filePath != null) {
            return new File(filePath);
        } else {
            return null;
        }
    }

    /**
     * Задаёт путь текущему загруженному файлу. Этот путь сохраняется
     * в реестре, специфичном для конкретной операционной системы.
     *
     * @param file - файл или null, чтобы удалить путь
     */
    public void setInfoBotFilePath(File file) {
        Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
        if (file != null) {
            prefs.put("filePath", file.getPath());

            // Обновление заглавия сцены.
            primaryStage.setTitle("InfoBot - " + file.getName());
        } else {
            prefs.remove("filePath");

            // Обновление заглавия сцены.
            primaryStage.setTitle("InfoBot");
        }
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("InfoBot");
        // Устанавливаем иконку приложения.
        this.primaryStage.getIcons().add(new Image("file:resources/images/infoBot.png"));

        initRootLayout();

        showBotOverview();

        rootLayoutController.handleStart();



    }

    /**
     * Открывает диалоговое окно для изменения деталей указанного адресата.
     * Если пользователь кликнул OK, то изменения сохраняются в предоставленном
     * объекте адресата и возвращается значение true.
     *
     * @param currencyApp - объект адресата, который надо изменить
     * @return true, если пользователь кликнул OK, в противном случае false.
     */
    public boolean showCurrencyAppEditDialog(CurrencyApp currencyApp) {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/CurrencyAppEditDialog.fxml"));
            AnchorPane page = loader.load();

            // Создаём диалоговое окно Stage.
            Stage dialogCurrencyStage = new Stage();
            dialogCurrencyStage.setTitle("Edit Currency");
            dialogCurrencyStage.initModality(Modality.WINDOW_MODAL);
            dialogCurrencyStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogCurrencyStage.setScene(scene);

            // Устанавливаем иконку приложения.
            dialogCurrencyStage.getIcons().add(new Image("file:resources/images/currencyApp.png"));

            // Передаём адресата в контроллер.
            CurrencyAppEditDialogController currencyAppEditDialogController = loader.getController();
            currencyAppEditDialogController.setDialogCurrencyStage(dialogCurrencyStage);
            currencyAppEditDialogController.setCurrencyApp(currencyApp);

            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogCurrencyStage.showAndWait();

            return currencyAppEditDialogController.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Загружает информацию об погоде из указанного файла.
     * Текущая информация об погоде будет заменена.
     *
     */
    public void loadInfoBotFromFile(File file) {
        try {
            JAXBContext context = JAXBContext
                    .newInstance(InfoBotAppListWrapper.class);
            Unmarshaller um = context.createUnmarshaller();

            // Чтение XML из файла и демаршализация.
            InfoBotAppListWrapper infoBotAppListWrapper = (InfoBotAppListWrapper) um.unmarshal(file);

            weatherAppData.clear();
            trafficAppData.clear();
            currencyAppData.clear();
            iqAirAppsData.clear();
            weatherAppData.addAll(infoBotAppListWrapper.getWeatherApps());
            trafficAppData.addAll(infoBotAppListWrapper.getTrafficApps());
            currencyAppData.addAll(infoBotAppListWrapper.getCurrencyApps());
            iqAirAppsData.addAll(infoBotAppListWrapper.getIqAirApps());

            // Сохраняем путь к файлу в реестре.
            setInfoBotFilePath(file);

        } catch (Exception e) { // catches ANY exception
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Не удалось загрузить данные");
            alert.setContentText("Не удалось загрузить данные из файла:\n" + file.getPath());

            alert.showAndWait();
        }
    }

    /**
     * Сохраняет текущую информацию об погоде в указанном файле.
     *
     */
    public void saveInfoBotToFile(File file) {
        try {
            JAXBContext context = JAXBContext
                    .newInstance(InfoBotAppListWrapper.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // Обёртываем наши данные об адресатах.
            InfoBotAppListWrapper infoBotAppListWrapper = new InfoBotAppListWrapper();
            infoBotAppListWrapper.setWeatherApps(weatherAppData);
            infoBotAppListWrapper.setTrafficApps(trafficAppData);
            infoBotAppListWrapper.setCurrencyApps(currencyAppData);
            infoBotAppListWrapper.setIqAirApps(iqAirAppsData);

            // Маршаллируем и сохраняем XML в файл.
            m.marshal(infoBotAppListWrapper, file);

            // Сохраняем путь к файлу в реестре.
            setInfoBotFilePath(file);
        } catch (Exception e) { // catches ANY exception
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Не удалось сохранить данные");
            alert.setContentText("Не удалось сохранить данные в файл:\n" + file.getPath());

            alert.showAndWait();
        }
    }

    // запуск обновления погоды, а так же проверка на коректность данных

    public int updateWeather() {
        String subject = "InfoBot. Нет значения для погоды";
        String textMail = "Добавте в словарь погоды новое значение для проекта ";
        boolean status;

        WeatherController weatherController = new WeatherController();
        weatherController.openWeather();

        return 1;


//        try {
//            for (int i = 0; i < getWeatherAppData().size(); i++) {
//                 url = getWeatherAppData().get(i).getUrl();
//                dayUpdate = getWeatherAppData().get(i).getDayUpdate();
//                nameStation = getWeatherAppData().get(i).getStationName();
//
//                idToday = getWeatherAppData().get(i).getIdToday();
//                idTodayImg = getWeatherAppData().get(i).getIdTodayImg();
//                blockTodayFormat = getWeatherAppData().get(i).getBlockTodayFormat();
//
//                idTomorrow = getWeatherAppData().get(i).getIdTomorrow();
//                idTomorrowImg = getWeatherAppData().get(i).getIdTomorrowImg();
//                blockTomorrowFormat = getWeatherAppData().get(i).getBlockTomorrowFormat();
//
//                time = getWeatherAppData().get(i).getTimeUpdate();
//                dictionaryFilePatch = getWeatherAppData().get(i).getDictionary();
//
//                // проверяем на действительность ссылки
//                if (url != null) {
//                    // обновление на сегодня
//                    if (getWeatherAppData().get(i).getDayUpdate().equals("today")) {
//                        weatherUpdate = weatherParser(getPage(url), "today");
//                        dictionary = parserDictionary(dictionaryFilePatch, weatherUpdate[2]);
//
//                        if (dictionary != null) {
//                            UpdateWeather updateWeather = new UpdateWeather(weatherUpdate[0], weatherUpdate[1], blockTodayFormat, dictionary);
//                            status = updateWeather.setWeather(idToday, idTodayImg);
//                            // если true то записываем время обновления
//                            if(status) {
//                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss  E yyy.MM.dd");
//                                botOverviewController.setTerminalWeather(nameStation + ":\nПогода на сегодня обновлена в - " +
//                                        simpleDateFormat.format(new Date()) + "\n" + weatherUpdate[0] + blockTodayFormat
//                                        + weatherUpdate[1] + " " + weatherUpdate[2] + "=" + dictionary + "\n");
//                                //если false выводим сообщение об не коректности обновления
//                            } else {
//                                botOverviewController.setTerminalWeather(nameStation + " Изменения не внесены в файл");
//                                errorMesseges = "Неверный путь к файлу в: " + nameStation;
//                                final String finalErrorMesseges = errorMesseges;
//                                Platform.runLater(() -> {
//                                    Alert alert = new Alert(Alert.AlertType.ERROR);
//                                    alert.setTitle("Error Weather");
//                                    alert.setHeaderText("Неверные данны");
//                                    alert.setContentText(finalErrorMesseges);
//
//                                    alert.showAndWait();
//                                });
//                                break;
//
//                            }
//                        }else {
//                            SendMail sendMail = new SendMail(subject, textMail + nameStation + ": " + weatherUpdate[2]);
//                                sendMail.send();
//
//
//                            botOverviewController.setTerminalWeather(nameStation + " Изменения не внесены в файл\n " +
//                                    "Нет значения погоды:\n" + weatherUpdate[2] + "=" + dictionary);
//
//                        }
//
//                        Thread.sleep(1000);
//
//                    }
//                    // обновления на завтра
//                    else if (dayUpdate.equals("tomorrow")) {
//                        weatherUpdate = weatherParser(getPage(url), "tomorrow");
//                        dictionary = parserDictionary(dictionaryFilePatch, weatherUpdate[2]);
//                        if (dictionary != null) {
//                            UpdateWeather updateWeather = new UpdateWeather(weatherUpdate[0], weatherUpdate[1], blockTomorrowFormat, dictionary);
//                            status = updateWeather.setWeather(idTomorrow, idTomorrowImg);
//
//                            if (status) {
//                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss  E yyy.MM.dd");
//                                botOverviewController.setTerminalWeather(nameStation + ":\nПогода на завтра обновлена в - " +
//                                        simpleDateFormat.format(new Date()) + "\n" + weatherUpdate[0] + blockTomorrowFormat +
//                                        weatherUpdate[1] + " " + weatherUpdate[2] + "=" + dictionary + "\n");
//                            } else {
//                                botOverviewController.setTerminalWeather(nameStation + " Изменения не внесены в файл");
//                                errorMesseges = "Неверный путь к файлу в: " + nameStation;
//                                final String finalErrorMesseges = errorMesseges;
//                                Platform.runLater(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        Alert alert = new Alert(Alert.AlertType.ERROR);
//                                        alert.setTitle("Error Weather");
//                                        alert.setHeaderText("Неверные данны");
//                                        alert.setContentText(finalErrorMesseges);
//
//                                        alert.showAndWait();
//                                    }
//                                });
//                                break;
//                            }
//
//                        } else {
//                            SendMail sendMail = new SendMail(subject, textMail + nameStation + ": " + weatherUpdate[2]);
//                                sendMail.send();
//
//                            botOverviewController.setTerminalWeather(nameStation + " Изменения не внесены в файл\n " +
//                                    "Нет значения погоды:\n" + weatherUpdate[2] + "=" + dictionary);
//                        }
//                        Thread.sleep(1000);
//                    } else {
//                        errorMesseges = dayUpdate + " - такого дня для обновления нету, введите доступные дни :\n today \n tomorrow";
//
//                        Alert alert = new Alert(Alert.AlertType.ERROR);
//                        alert.setTitle("Error Weather");
//                        alert.setHeaderText("Неверные данны");
//                        alert.setContentText(errorMesseges);
//
//                        alert.showAndWait();
//                    }
//                }
//            }
//
//        } catch (final Exception e) {
//            errorMesseges = "Проверьте пожалуйста введенную ссылку в: " + nameStation;
//            String finalErrorMesseges = errorMesseges;
//            Platform.runLater(new Runnable() {
//                @Override
//                public void run() {
//                    Alert alert = new Alert(Alert.AlertType.ERROR);
//                    alert.setTitle("Error Weather");
//                    alert.setHeaderText("Неверные данные");
//                    alert.setContentText(e.toString());
//
//                    alert.showAndWait();
//                }
//            });
//
//
//        }
//            return time;
    }
    // возвращает страницу сайта погоды
    public Document getPage(String url) throws IOException {
        return Jsoup.connect(url).timeout(50000).userAgent("Mozilla").get(); //возвращает скачаную страницу

    }

    // запуск обновления пробок, а так же проверка на коректность данных
    public int updateTraffic(){

        String url;
        String filePatch;
        String nameStation = null;
        String errorMesseges;
        String trafficUpdate;
        String dictionaryFilePatch;
        String dictionary;

        String subject = "InfoBot. Нет значения для пробок";
        String textMail = "Добавте в словарь пробок новое значение для проекта ";

        String blockTrafficImg = null;
        boolean status;
        int time = 0;
        try{
            for (int i = 0; i < getTrafficAppData().size(); i++) {
                url = getTrafficAppData().get(i).getUrl();
                filePatch = getTrafficAppData().get(i).getFilePath();
                nameStation = getTrafficAppData().get(i).getStationName();
                blockTrafficImg = getTrafficAppData().get(i).getBlockTrafficImg();
                time = getTrafficAppData().get(i).getTimeUpdate();
                dictionaryFilePatch = getTrafficAppData().get(i).getDictionary();

                if (url != null){
                    trafficUpdate = parserTraffic(getDocumentTrafficUrl(url));
                    dictionary = parserDictionary(dictionaryFilePatch, trafficUpdate);
                    if (dictionary != null) {
                        UpdateTraffic updateTraffic = new UpdateTraffic(dictionary);
                        status = updateTraffic.setXmlTraffic(filePatch, blockTrafficImg);
                        if (status) {
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss  E yyy.MM.dd");
                            botOverviewController.setTerminalTraffic(nameStation + ":\n Пробки обновлены в - " +
                                    simpleDateFormat.format(new Date()) + "\n" + "Уровень пробок: " + trafficUpdate + "=" + dictionary +"\n");
                        } else {
                            botOverviewController.setTerminalTraffic(nameStation + " Изменения не внесены в файл");
                            errorMesseges = "Неверный путь к файлу в: " + nameStation;
                            final String finalErrorMesseges = errorMesseges;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    Alert alert = new Alert(Alert.AlertType.ERROR);
                                    alert.setTitle("Error Traffic");
                                    alert.setHeaderText("Неверные данны");
                                    alert.setContentText(finalErrorMesseges);

                                    alert.showAndWait();
                                }
                            });
                            break;
                        }
                    }else{
                        if(!trafficUpdate.equals("")){
                            SendMail sendMail = new SendMail(subject, textMail + nameStation + ": " + trafficUpdate);
                            sendMail.send();
                        }


                        botOverviewController.setTerminalTraffic(nameStation + " Изменения не внесены в файл\n " +
                                "Нет значения пробок:\n" +trafficUpdate + "=" + dictionary);

                    }
                }

            }
        } catch (NoSuchWindowException we){
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Traffic");
                    alert.setHeaderText("Error");
                    alert.setContentText("Неожиданно закрылся браузер");

                    alert.showAndWait();
                }
            });
        }
        catch (final Exception e){

            errorMesseges = "Проверьте пожалуйста введенную ссылку в: " + nameStation;
            String finalErrorMesseges = errorMesseges;
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Traffic");
                    alert.setHeaderText("Неверные данные");
                    alert.setContentText(e.toString());

                    alert.showAndWait();
                }
            });
        }
        return time;
    }

    public int updateIqAir(){
        String nameStation = null;
        String pathDictionary;
        String url;
        String adress;
        long idText;
        long idImg;
        long idStation;

        String errorMesseges = null;

        boolean status;
        int time = 0;
        try{
            for (int i = 0; i < getIqAirAppsData().size(); i++) {
                nameStation = getIqAirAppsData().get(i).getStationName();
                pathDictionary = getIqAirAppsData().get(i).getFilePath();
                url = getIqAirAppsData().get(i).getUrl();
                adress = getIqAirAppsData().get(i).getAddress();
                idText = getIqAirAppsData().get(i).getIdText();
                idImg = getIqAirAppsData().get(i).getIdImage();
                time = getIqAirAppsData().get(i).getTimeUpdate();
                idStation = getIqAirAppsData().get(i).getIdStationName();

                if (url != null){

                    int iqAir = getValueIqAir(url, adress); //https://api.waqi.info/search/?token=8223741dde8d16d8789f37822cd3f1daeff18329&keyword=Odessa
                    if (iqAir != 0) {
                        UpdateIQAir updateIQAir = new UpdateIQAir(iqAir);
                        status = updateIQAir.update(idText, pathDictionary, idStation, idImg);
                        if (status) {
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss  E yyy.MM.dd");
                            botOverviewController.setTerminalIqair(nameStation + ":\n Индекс воздуха обновлен в - " +
                                    simpleDateFormat.format(new Date()) + "\n" + "Индекс: " + iqAir);
                        } else {
                            botOverviewController.setTerminalIqair(nameStation + " Изменения не внесены в файл");
                            errorMesseges = "Неверный путь к файлу в: " + nameStation;
                            final String finalErrorMesseges = errorMesseges;
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    Alert alert = new Alert(Alert.AlertType.ERROR);
                                    alert.setTitle("Error IQAir");
                                    alert.setHeaderText("Неверные данны");
                                    alert.setContentText(finalErrorMesseges);

                                    alert.showAndWait();
                                }
                            });
                            break;
                        }
                    }else{
                        botOverviewController.setTerminalIqair(nameStation + " Изменения не внесены в файл\n " +
                                "Нет значения индекс:\n" + iqAir);

                    }
                }
                Thread.sleep(1000);
            }
        } catch (NoSuchWindowException we){
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error IQAir");
                alert.setHeaderText("Error");
                alert.setContentText("Неожиданно закрылся браузер");

                alert.showAndWait();
            });
        }
        catch (final Exception e){

            errorMesseges = "Проверьте пожалуйста введенную ссылку в: " + nameStation;
            String finalErrorMesseges = errorMesseges;
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error IQAir");
                alert.setHeaderText("Неверные данные");
                alert.setContentText(e.toString());

                alert.showAndWait();
            });
        }
        return time;
    }
    // парсим пробки
    public String parserTraffic(Document page){
        Element element = page.select("div[class=traffic-control__icon-text]").first();
        String traffic = element.text();
        return traffic;
    }

    public int updateCurrency(){
        String url;
        String typeUpdate;
        String nameStation = null;

        long USD;
        long EUR;
        long RUS;
        long PLN;

        long buyUSD;
        long saleUSD;
        long buyEUR;
        long saleEUR;
        long buyRUS;
        long saleRUS;
        String errorMesseges;

        String[] nbu;
        String[] cash;


        boolean status;
        int time = 0;
        try {
            for (int i = 0; i < getCurrencyAppData().size(); i++) {
                url = getCurrencyAppData().get(i).getUrl();
                nameStation = getCurrencyAppData().get(i).getStationName();
                time = getCurrencyAppData().get(i).getTimeUpdate();
                typeUpdate = getCurrencyAppData().get(i).getUpdateCurrency();

                USD = getCurrencyAppData().get(i).getIdNbuUSD();
                EUR = getCurrencyAppData().get(i).getIdNbuEUR();
                RUS = getCurrencyAppData().get(i).getIdNbuRU();
                PLN = getCurrencyAppData().get(i).getIdNbuPLN();

                buyUSD = getCurrencyAppData().get(i).getIdCashBuyUSD();
                buyEUR = getCurrencyAppData().get(i).getIdCashBuyEUR();
                buyRUS = getCurrencyAppData().get(i).getIdCashBuyRU();
                saleUSD = getCurrencyAppData().get(i).getIdCashSaleUSD();
                saleEUR = getCurrencyAppData().get(i).getIdCashSaleEUR();
                saleRUS = getCurrencyAppData().get(i).getIdCashSaleRU();




            if (url != null) {
                if (typeUpdate.equals("nbu")) {
                    nbu = currencyValueNBU(getPage(url));
                    UpdateCurrencyNBU updateCurrencyNBU = new UpdateCurrencyNBU(nbu[0], nbu[1], nbu[2], nbu[3]);
                    status = updateCurrencyNBU.setXmlCurrency(USD, EUR, RUS, PLN);

                    if (status) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss  E yyy.MM.dd");
                        botOverviewController.setTerminalCurrency(nameStation + ":\n Курс НБУ обновлен в - " +
                                simpleDateFormat.format(new Date()) + "\n" + "USD: " + nbu[0] + "\n" +
                                "EUR: " + nbu[1] + "\n" + "RUS: " + nbu[2] + "\n" + "PLN: " + nbu[3] + "\n");
                    } else {
                        botOverviewController.setTerminalCurrency(nameStation + " Изменения не внесены в файл");
                        errorMesseges = "Неверный путь к файлу в: " + nameStation;
                        final String finalErrorMesseges = errorMesseges;
                        Platform.runLater(() -> {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Error Currency");
                            alert.setHeaderText("Неверные данны");
                            alert.setContentText(finalErrorMesseges);

                            alert.showAndWait();
                        });
                        break;
                    }

                }

                else if (typeUpdate.equals("cash")) {
                    cash = currencyValueCash(getPage(url));
                    UpdateCurrencyCash updateCurrencyCash = new UpdateCurrencyCash(cash[0], cash[1], cash[2], cash[3], cash[4], cash[5]);
                    status = updateCurrencyCash.setXmlCurrency(buyUSD, saleUSD, buyEUR, saleEUR, buyRUS, saleRUS);

                    if (status) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss  E yyy.MM.dd");
                        botOverviewController.setTerminalCurrency(nameStation + ":\n Наличный курс обновлен в - " +
                                simpleDateFormat.format(new Date()) + "\n" + "USD: " + cash[0] + "\n" + cash[1] + "\n" +
                                "EUR: " + cash[2] + "\n" +  cash[3] + "\n" + "RUS: " + cash[4] + "\n" + cash[5] + "\n");
                    } else {
                        botOverviewController.setTerminalCurrency(nameStation + " Изменения не внесены");
                        break;
                    }


                } else {
                    errorMesseges = typeUpdate + " - такого типа курса для обновления нету, введите доступные типы :\n nbu \n cash";

                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Currency");
                    alert.setHeaderText("Неверные данны");
                    alert.setContentText(errorMesseges);

                    alert.showAndWait();
                }

            }
                Thread.sleep(1000);
        }


    } catch (final Exception e){

            errorMesseges = "Проверьте пожалуйста введенную ссылку в: " + nameStation;
            String finalErrorMesseges = errorMesseges;
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Currency");
                    alert.setHeaderText("Неверные данны");
                    alert.setContentText(e.toString());

                    alert.showAndWait();
                }
            });
        }

        return time;

    }

    // возвращает страницу сайта пробок
    public Document getDocumentTrafficUrl (String url){
        System.setProperty ("webdriver.gecko.driver",
                System.getProperty ("user.dir") + "/geckodriver.exe");
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--headless");
        driver =  new FirefoxDriver(options);

        driver.get(url);
        Document document = Jsoup.parse(driver.getPageSource());
        driver.quit();
        return document;

    }
    public void stopWebDriver(){
        if(driver!=null) {
            driver.close();
        }
    }

    //парсим файл словаря
    public String parserDictionary(String dictionaryFilePatch, String str) {
        String[] tmp;
        String key;
        String value;
        String temp = null;
        HashMap<String, String> weatherDictionaryList = new HashMap<>();

        try {
            FileInputStream fr = new FileInputStream(dictionaryFilePatch);
            InputStreamReader inChars = new InputStreamReader(fr,"UTF-8");
            Scanner sc = new Scanner(inChars);
            while (sc.hasNextLine()) {
                String s = sc.nextLine();
                tmp = s.split("=");
                key = tmp[0];
                value = tmp[1];
                weatherDictionaryList.put(key, value);
            }
            for (Map.Entry entry : weatherDictionaryList.entrySet()){
                if(entry.getKey().equals(str)){
                    temp = entry.getValue().toString();

                }


            }
        } catch (final IOException e) {
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR");
                alert.setHeaderText("Неверные данны");
                alert.setContentText(e.toString());

                alert.showAndWait();
            });
        }

        return temp;


    }

    //парсим курсНБУ
    public String[] currencyValueNBU(Document page) {
        String[] nbu = new String[4];
        Element valueNBU = page.select("div[class=curtable]").first(); //место расположение значений курса НБУ
        Elements value = valueNBU.select("span[class=index]"); //Отбираем значеныя курса
        for (Element element : value){
            if (element.text().equals("USD")){
                Element element1 = element.parent().parent().parent();
                try {
                    double count = Double.parseDouble(element1.select("td[class=value down]").text());
                    nbu[0] = String.valueOf(count).substring(0, 5);
                    System.out.println(nbu[0]);
                }catch (NumberFormatException n){
                    try {
                        double count2 = Double.parseDouble(element1.select("td[class=value up]").text());
                        nbu[0] = String.valueOf(count2).substring(0, 5);
                        System.out.println(nbu[0]);
                    } catch (NumberFormatException n2){
                        double count2 = Double.parseDouble(element1.select("td[class=value]").text());
                        nbu[0] = String.valueOf(count2).substring(0, 5);
                        System.out.println(nbu[0]);
                    }

                }
            }
            if (element.text().equals("EUR")){
                Element element1 = element.parent().parent().parent();
                try {
                    double count = Double.parseDouble(element1.select("td[class=value down]").text());
                    nbu[1] = String.valueOf(count).substring(0, 5);
                    System.out.println(nbu[1]);
                }catch (NumberFormatException n){
                    try {
                        double count2 = Double.parseDouble(element1.select("td[class=value up]").text());
                        nbu[1] = String.valueOf(count2).substring(0, 5);
                        System.out.println(nbu[1]);
                    } catch (NumberFormatException n2){
                        double count2 = Double.parseDouble(element1.select("td[class=value]").text());
                        nbu[1] = String.valueOf(count2).substring(0, 5);
                        System.out.println(nbu[1]);
                    }

                }
            }
            if (element.text().equals("RUB")){
                Element element1 = element.parent().parent().parent();
                try {
                    double count = Double.parseDouble(element1.select("td[class=value down]").text());
                    nbu[2] = String.valueOf(count / 10).substring(0, 5);
                    System.out.println(nbu[2]);
                }catch (NumberFormatException n){
                    try {
                        double count2 = Double.parseDouble(element1.select("td[class=value up]").text());
                        nbu[2] = String.valueOf(count2 / 10).substring(0, 5);
                        System.out.println(nbu[2]);
                    } catch (NumberFormatException n2){
                        double count2 = Double.parseDouble(element1.select("td[class=value]").text());
                        nbu[2] = String.valueOf(count2 / 10).substring(0, 5);
                        System.out.println(nbu[2]);
                    }

                }
            }
            if (element.text().equals("PLN")){
                Element element1 = element.parent().parent().parent();
                try {
                    double count = Double.parseDouble(element1.select("td[class=value down]").text());
                    nbu[3] = String.valueOf(count).substring(0, 5);
                    System.out.println(nbu[3]);
                }catch (NumberFormatException n){
                    try {
                        double count2 = Double.parseDouble(element1.select("td[class=value up]").text());
                        nbu[3] = String.valueOf(count2).substring(0, 5);
                        System.out.println(nbu[3]);
                    } catch (NumberFormatException n2){
                        double count2 = Double.parseDouble(element1.select("td[class=value]").text());
                        nbu[3] = String.valueOf(count2).substring(0, 5);
                        System.out.println(nbu[3]);
                    }

                }
            }


        }

        return nbu;

    }

    public String[] currencyValueCash(Document page) throws Exception {
        Element valueCash = page.select("table[class=b-market-table_currency-cash]").first(); //место расположение значений курса НБУ
        Elements valueUSDandEUR = valueCash.select("tr[class=major]"); //Отбираем значеныя курса
        Elements valueRUS = valueCash.select("tr[class=major major-last]");
        String valueStringBuyUSDandEUR = valueUSDandEUR.select("td[class=c2]").text(); //записываем значения в строку
        String valueStringSaleUSDandEUR = valueUSDandEUR.select("td[class=c3]").text();
        String valueStringBuyRUS = valueRUS.select("td[class=c2]").text();
        String valueStringSaleRUS = valueRUS.select("td[class=c3]").text();
        String[] valuesBuyUSDandEUR = getValueCurrency(valueStringBuyUSDandEUR); // обрабатыем полученую строку и ищем нужные значения
        String[] valuesSaleUSDandEUR = getValueCurrency(valueStringSaleUSDandEUR);

        String[] cashs = new String[6];
        cashs[0] = valuesBuyUSDandEUR[0].substring(0,5);
        cashs[1] = valuesSaleUSDandEUR[0].substring(0, 5);
        cashs[2] = valuesBuyUSDandEUR[1].substring(0,5);
        cashs[3] = valuesSaleUSDandEUR[1].substring(0,5);
        cashs[4] = valueStringBuyRUS.substring(0,5);
        cashs[5] = valueStringSaleRUS.substring(0,5);

        return cashs;


    }

    public int getValueIqAir(String url, String stationName){
        int index = 0;
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(url) //"https://api.saveecobot.com/output.json"
                .method("GET", null)
                .build();
        Response response = null;

        try {
            response = client.newCall(request).execute();


        } catch (IOException e) {
            e.printStackTrace();
        }
        String temp = null;
        try {
            temp = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(temp);
        List<Map<String, Object>> list = JsonPath.parse(document).read("$");
        for (int i = 0; i < list.size(); i++) {
            String stationNames = JsonPath.parse(list.get(i)).read("stationName");
            if (stationNames.equals(stationName)) {
                List<Map<String, Object>> pollutants = JsonPath.parse(list.get(i)).read("$..pollutants[*]");
                for (int j = 0; j < pollutants.size(); j++) {
                    Map<String, Object> pollutant = pollutants.get(j);
                    Pollutant pollutant1 = new Pollutant();
                    for (Map.Entry<String, Object> item: pollutant.entrySet()){
                        switch (item.getKey()){
                            case "pol": pollutant1.setPol(item.getValue().toString());
                            break;
                            case "unit": pollutant1.setUnit(item.getValue().toString());
                                break;
                            case "time": pollutant1.setTime(item.getValue().toString());
                                break;
                            case "value": pollutant1.setValue(item.getValue().toString());
                                break;
                            case "averaging": pollutant1.setAveraging(item.getValue().toString());
                                break;
                        }
                    }
                    if (pollutant1.getPol().equals("Air Quality Index")){
                        String[] dates = pollutant1.getTime().split(" ");
                        Date dateNow = new Date();
                        SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy-MM-dd");

                        if (formatForDateNow.format(dateNow).equals(dates[0])){
                            index = Integer.parseInt(pollutant1.getValue());
                        }
                    }


                }

            }
        }

        return index;
    }



}
