package main.java.yaroslav.khahyda.address;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import main.java.yaroslav.khahyda.address.model.image.Image;
import main.java.yaroslav.khahyda.address.model.image.ImageWrapper;
import main.java.yaroslav.khahyda.address.model.text.LayerImage;
import main.java.yaroslav.khahyda.address.model.text.LayerText;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class UpdateIQAir {
    private int iQAir;
    private String color;
    private boolean status = true;

    public boolean update(long idText, String pathDictionary, long idStation, long idImg){
        UpdateLayerText updateLayerText = new UpdateLayerText();
        UpdateLayerImg updateLayerImg = new UpdateLayerImg();
        LayerText layerText = updateLayerText.getLayerText(idText);
        LayerText layerStationText = updateLayerText.getLayerText(idStation);
        LayerImage layerImage = updateLayerImg.getLayerImg(idImg);
        if (iQAir <= 50){
            if (layerImage != null) {
                if (layerText != null) {
                    if (layerStationText != null) {

                        layerImage.getFileInfo().setId(Long.parseLong(parserDictionary(pathDictionary, ColorIqAir.GREEN.name())));
                        String hex = String.format("#%02x%02x%02x", Color.WHITE.getRed(), Color.WHITE.getGreen(), Color.WHITE.getBlue());
                        layerText.setColor(hex);
                        layerText.setText(String.valueOf(iQAir));
                        layerStationText.setColor(hex);

                        updateLayerText.updateText(layerText, idText);
                        updateLayerText.updateText(layerStationText, idStation);
                        updateLayerImg.updateImg(layerImage, idImg);
                        status = true;
                    } else {
                        status = false;
                    }
                } else {
                    status = false;
                }
            } else {
                status = false;
            }

        } else if (iQAir <= 100){
            if (layerImage != null) {
                if (layerText != null) {
                    if (layerStationText != null) {
                        layerImage.getFileInfo().setId(Long.parseLong(parserDictionary(pathDictionary, ColorIqAir.YELLOW.name())));
                        System.out.println(layerImage.getFileInfo().getId());
                        String hex = String.format("#%02x%02x%02x", Color.BLACK.getRed(), Color.BLACK.getGreen(), Color.BLACK.getBlue());
                        layerText.setColor(hex);
                        layerText.setText(String.valueOf(iQAir));
                        layerStationText.setColor(hex);

                        updateLayerText.updateText(layerText, idText);
                        updateLayerText.updateText(layerStationText, idStation);
                        updateLayerImg.updateImg(layerImage, idImg);
                        status = true;
                    } else {
                        status = false;
                    }
                } else {
                    status = false;
                }
            } else {
                status = false;
            }
        } else if (iQAir <= 150){
            if (layerImage != null) {
                if (layerText != null) {
                    if (layerStationText != null) {
                        layerImage.getFileInfo().setId(Long.parseLong(parserDictionary(pathDictionary, ColorIqAir.ORANGE.name())));
                        String hex = String.format("#%02x%02x%02x", Color.BLACK.getRed(), Color.BLACK.getGreen(), Color.BLACK.getBlue());
                        layerText.setColor(hex);
                        layerText.setText(String.valueOf(iQAir));
                        layerStationText.setColor(hex);

                        updateLayerText.updateText(layerText, idText);
                        updateLayerText.updateText(layerStationText, idStation);
                        updateLayerImg.updateImg(layerImage, idImg);
                        status = true;
                    } else {
                        status = false;
                    }
                } else {
                    status = false;
                }
            } else {
                status = false;
            }
        } else if (iQAir <= 200){
             if (layerImage != null) {
                 if (layerText != null) {
                     if (layerStationText != null) {
                         layerImage.getFileInfo().setId(Long.parseLong(parserDictionary(pathDictionary, ColorIqAir.RED.name())));
                         String hex = String.format("#%02x%02x%02x", Color.WHITE.getRed(), Color.WHITE.getGreen(), Color.WHITE.getBlue());
                         layerText.setColor(hex);
                         layerText.setText(String.valueOf(iQAir));
                         layerStationText.setColor(hex);

                         updateLayerText.updateText(layerText, idText);
                         updateLayerText.updateText(layerStationText, idStation);
                         updateLayerImg.updateImg(layerImage, idImg);
                         status = true;
                     } else {
                         status = false;
                     }
                 } else {
                     status = false;
                 }
             } else {
                 status = false;
             }
        } else if (iQAir <= 300){
            if (layerImage != null) {
                if (layerText != null) {
                    if (layerStationText != null) {
                        layerImage.getFileInfo().setId(Long.parseLong(parserDictionary(pathDictionary, ColorIqAir.PURPLE.name())));
                        String hex = String.format("#%02x%02x%02x", Color.WHITE.getRed(), Color.WHITE.getGreen(), Color.WHITE.getBlue());
                        layerText.setColor(hex);
                        layerText.setText(String.valueOf(iQAir));
                        layerStationText.setColor(hex);

                        updateLayerText.updateText(layerText, idText);
                        updateLayerText.updateText(layerStationText, idStation);
                        updateLayerImg.updateImg(layerImage, idImg);
                        status = true;
                    } else {
                        status = false;
                    }
                } else {
                    status = false;
                }
            } else {
                status = false;
            }
        }






        return status;


    }

    public UpdateIQAir(int iQAir) {
        this.iQAir = iQAir;
    }

    //парсим файл словаря
    public String parserDictionary(String dictionaryFilePatch, String color) {
        String[] tmp = new String[2];
        String key;
        String value;
        String temp = null;
        HashMap<String, String> weatherDictionaryList = new HashMap<String, String>();

        try {
            FileInputStream fr = new FileInputStream(dictionaryFilePatch);
            InputStreamReader inChars = new InputStreamReader(fr,"UTF-8");
            Scanner sc = new Scanner(inChars);
            while (sc.hasNextLine()) {
                String s = sc.nextLine();
                tmp = s.split("=");
                key = tmp[0];
                value = tmp[1];
                weatherDictionaryList.put(key, value);
            }
            for (Map.Entry entry : weatherDictionaryList.entrySet()){
                if(entry.getKey().equals(color)){
                    temp = entry.getValue().toString();

                }


            }
        } catch (final IOException e) {
            Platform.runLater(() -> {
                System.out.println("" + e);
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR");
                alert.setHeaderText("Неверные данны");
                alert.setContentText(e.toString());

                alert.showAndWait();
            });
        }

        return temp;


    }



    enum ColorIqAir{
        GREEN,
        YELLOW,
        ORANGE,
        RED,
        PURPLE
    }

}
