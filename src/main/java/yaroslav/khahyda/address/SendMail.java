package main.java.yaroslav.khahyda.address;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class SendMail {
    private Properties props;
    private String subject;
    private String text;
    private JsonParserMail jsonParserMail = new JsonParserMail();

    public SendMail(String subject, String text) {
        this.subject = subject;
        this.text = text;

        props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
    }

    public void send(){

        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(jsonParserMail.getFrom(), jsonParserMail.getPass());
            }
        });

        try {
            MimeMessage message = new MimeMessage(session);
            //от кого
            message.setFrom(new InternetAddress(jsonParserMail.getFrom()));
            //кому
            String toEmail = "";
            for (int i = 0; i < jsonParserMail.getTo().size(); i++) {
                toEmail += JsonParserMail.getTo().get(i) + ",";
            }
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            //Заголовок письма
            message.setSubject(subject, "koi8-r");
            //Содержимое
            message.setText(text, "koi8-r");

            //Отправляем сообщение
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

    }


}
