package main.java.yaroslav.khahyda.address;

import java.net.HttpURLConnection;
import java.net.URL;

public class CheckInternetConnection {
    private static String url;
    private static String status;

    public static String getStatus() {
        return status;
    }

    public static void setStatus(String status) {
        CheckInternetConnection.status = status;
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        CheckInternetConnection.url = url;
    }
    //проверка интернет соединения
    public static boolean checkInternet(String url) {
        Boolean result = false;
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) new URL(url).openConnection();
            con.setRequestMethod("HEAD");
            result = (con.getResponseCode() == HttpURLConnection.HTTP_OK);
            //если нет соединения записываем информацию.
        } catch (Exception e) {
            setStatus("no connection: отсутствует интернет или связь с сайтом.");


        }
        return result;
    }

}

