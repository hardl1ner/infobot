package main.java.yaroslav.khahyda.address.conf;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import lombok.Data;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Data
public class AppConf {
    private String url;
    private String username;
    private String password;
    private String apiWeatherKey;
    private String apiWeatherUrl;


    public AppConf() {
        readConfig();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void readConfig() {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream("app.properties");
        Properties appProps = new Properties();
        try {
            appProps.load(in);
            setUrl(appProps.getProperty("URL_CMS"));
            setUsername(appProps.getProperty("EMAIL_CMS"));
            setPassword(appProps.getProperty("PASSWORD_CMS"));
            setApiWeatherKey(appProps.getProperty("API_WEATHER_KEY"));
            setApiWeatherUrl(appProps.getProperty("API_WEATHER_URL"));
        } catch (IOException e) {
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error");
                alert.setContentText("" + e);

                alert.showAndWait();
            });
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                Platform.runLater(() -> {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error");
                    alert.setContentText("" + e);

                    alert.showAndWait();
                });
            }
        }
    }
}