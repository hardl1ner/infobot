package main.java.yaroslav.khahyda.address;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class UpdateTraffic {
    private String traffic;
    private boolean status = true;
    public UpdateTraffic(String traffic) {
        this.traffic = traffic;
    }
    // изменяем значение существующего элемента name
    private void updateElementValue(Document doc, String blocTraffic) {

        NodeList languages = doc.getElementsByTagName("TextBlock");
        Element lang = null;
        // проходим по каждому элементу Language
        for(int i=0; i<languages.getLength();i++) {
            lang = (Element) languages.item(i);
            Node name = lang.getElementsByTagName("name").item(0).getFirstChild();
            name.setNodeValue(name.getNodeValue());
            String str = name.getNodeValue();
            if (str.equals(blocTraffic)) { // если найден нужный елемен, то выполнается запись значения
                Node value = lang.getElementsByTagName("value").item(0).getFirstChild();
                value.setNodeValue(traffic);// изменение значения в тегу value

            }
        }

    }

    public Boolean setXmlTraffic(String filePath, String blockTraffic){

        File xmlFile = new File(filePath);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(xmlFile);
            doc.getDocumentElement().normalize();

            // обновляем значения
            updateElementValue(doc, blockTraffic);


            // запишем отредактированный элемент в файл
            // или выведем в консоль
            doc.getDocumentElement().normalize();
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filePath));
            transformer.transform(source, result);


        } catch (Exception exc) {
            exc.printStackTrace();
            status = false;
        }
        return status;
    }

}
