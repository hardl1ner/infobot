package main.java.yaroslav.khahyda.address.controller;

import com.github.prominence.openweathermap.api.OpenWeatherMapClient;
import com.github.prominence.openweathermap.api.enums.Language;
import com.github.prominence.openweathermap.api.enums.UnitSystem;
import com.github.prominence.openweathermap.api.model.forecast.Forecast;
import com.github.prominence.openweathermap.api.model.weather.Weather;
import main.java.yaroslav.khahyda.address.conf.AppConf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WeatherController {
    private AppConf appConf = new AppConf();

    public String getTodayWeather(String city) throws IOException {
        // Replace "YOUR_API_KEY" with your actual API key
        String apiUrl = appConf.getApiWeatherUrl() + "key=" + appConf.getApiWeatherKey()
                + "&q=" + city +"&days=1&aqi=no&alerts=no";

        // Create a URL object with the API endpoint
        URL url = new URL(apiUrl);

        // Open a connection to the URL
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        // Set the request method to GET
        connection.setRequestMethod("GET");

        // Get the response code
        int responseCode = connection.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_OK) {
            // Create a BufferedReader to read the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            StringBuilder response = new StringBuilder();

            // Read the response line by line
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();

            // Process the response data
            return response.toString();
        } else {
            return "Error: " + responseCode;
        }
    }

    public void openWeather(){
        OpenWeatherMapClient openWeatherClient = new OpenWeatherMapClient("15dc992bd8dc04cab45c27c572f15c0c");
        Forecast weather = openWeatherClient.forecast5Day3HourStep().byCityName("Kyiv").language(Language.ENGLISH).unitSystem(UnitSystem.METRIC).retrieve().asJava();
        weather.getWeatherForecasts().forEach(weatherForecast -> {
            System.out.println(weatherForecast.getForecastTime());
            System.out.println(weatherForecast.getWind());
            System.out.println(weatherForecast.getTemperature());
        });
    }

}
