package main.java.yaroslav.khahyda.address;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import lombok.Data;
import main.java.yaroslav.khahyda.address.conf.AppConf;
import main.java.yaroslav.khahyda.address.model.text.LayerText;
import org.apache.commons.codec.binary.Base64;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Data
public class UpdateCurrencyCash {
    private String buyUSD;
    private String saleUSD;
    private String buyEUR;
    private String saleEUR;
    private String buyRUS;
    private String saleRUS;
    private AppConf appConf;

    private boolean status = true;

    public UpdateCurrencyCash(String buyUSD, String saleUSD, String buyEUR, String saleEUR, String buyRUS, String saleRUS) {
        this.buyUSD = buyUSD;
        this.saleUSD = saleUSD;
        this.buyEUR = buyEUR;
        this.saleEUR = saleEUR;
        this.buyRUS = buyRUS;
        this.saleRUS = saleRUS;
        appConf = new AppConf();
    }


    public Boolean setXmlCurrency(long idBuyUSD, long idSaleUSD, long idBuyEUR, long idSaleEUR, long idBuyRUS, long idSaleRUS){
        UpdateLayerText updateLayerText = new UpdateLayerText();
        Map<Long, String> currency = new HashMap<>();
        if (idBuyUSD != 0) {
            currency.put(idBuyUSD, buyUSD);
        }
        if (idSaleUSD != 0) {
            currency.put(idSaleUSD, saleUSD);
        }
        if (idBuyEUR != 0) {
            currency.put(idBuyEUR, buyEUR);
        }
        if (idSaleEUR != 0) {
            currency.put(idSaleEUR, saleEUR);
        }
        if (idBuyRUS != 0) {
            currency.put(idBuyRUS, buyRUS);
        }
        if (idSaleRUS != 0) {
            currency.put(idSaleRUS, saleRUS);
        }

        if (currency.size() > 0) {
            currency.forEach((key, value) -> {
                LayerText layerText = updateLayerText.getLayerText(key);
                if (layerText != null) {
                    layerText.setText(value);
                    status = updateLayerText.updateText(layerText, key);
                } else {
                    status = false;
                }
            });
        } else {
            status = false;
        }

        return status;
    }

}
