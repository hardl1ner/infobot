package main.java.yaroslav.khahyda.address;



import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JsonParserMail {
    private static final String FILENAME = System.getProperty("user.dir") + "/mailSetting.json";
    private static String pass;
    private static String from;
    private static ArrayList<String> to;
    public JsonParserMail(){
        parserMailJson(FILENAME);
    }

    public static String getPass() {
        return pass;
    }

    public static void setPass(String pass) {
        JsonParserMail.pass = pass;
    }

    public static String getFrom() {
        return from;
    }

    public static void setFrom(String from) {
        JsonParserMail.from = from;
    }

    public static ArrayList<String> getTo() {
        return to;
    }

    public static void setTo(ArrayList<String> to) {
        JsonParserMail.to = to;
    }

    public static void parserMailJson(String fileName){
        JSONParser parser = new JSONParser();
        try {
            JSONObject object = (JSONObject)parser.parse(new FileReader(FILENAME));
            setPass((String)object.get("pass"));
            setFrom((String)object.get("from"));
            setTo((ArrayList<String>)object.get("to"));
        }
        catch (IOException | ParseException ex){
            Logger.getLogger(JsonParserMail.class.getName()).log(Level.SEVERE,null, ex);
        }


    }


}
