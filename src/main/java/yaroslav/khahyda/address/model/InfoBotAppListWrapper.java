package main.java.yaroslav.khahyda.address.model;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * Вспомогательный класс для обёртывания.
 * Используется для сохранения  в XML.
 *
 * @author Yaroslav Khakhyda
 */
@XmlRootElement(name = "InfoBotApps")
public class InfoBotAppListWrapper {
    private List<WeatherApp> weatherApps;
    private List<TrafficApp> trafficApps;
    private List<CurrencyApp> currencyApps;
    private List<IQAirApp> iqAirApps;

    @XmlElement(name = "weatherApp")
    public List<WeatherApp> getWeatherApps() {
        return weatherApps;
    }

    public void setWeatherApps(List<WeatherApp> weatherApps) {
        this.weatherApps = weatherApps;
    }
    @XmlElement(name = "trafficApp")
    public List<TrafficApp> getTrafficApps() {
        return trafficApps;
    }

    public void setTrafficApps(List<TrafficApp> trafficApps) {
        this.trafficApps = trafficApps;
    }
    @XmlElement(name = "currencyApp")
    public List<CurrencyApp> getCurrencyApps() {
        return currencyApps;
    }

    public void setCurrencyApps(List<CurrencyApp> currencyApps) {
        this.currencyApps = currencyApps;
    }

    public List<IQAirApp> getIqAirApps() {
        return iqAirApps;
    }

    public void setIqAirApps(List<IQAirApp> iqAirApps) {
        this.iqAirApps = iqAirApps;
    }
}
