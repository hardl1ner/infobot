package main.java.yaroslav.khahyda.address.model.image;

public class Image {
    private String nameImage;
    private boolean on;
    private String name;
    private String imageDirDownload;
    private int x;
    private int y;
    private int width;
    private int height;

    public Image() {
    }

    public String getNameImage() {
        return nameImage;
    }

    public void setNameImage(String nameImage) {
        this.nameImage = nameImage;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageDirDownload() {
        return imageDirDownload;
    }

    public void setImageDirDownload(String imageDirDownload) {
        this.imageDirDownload = imageDirDownload;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
