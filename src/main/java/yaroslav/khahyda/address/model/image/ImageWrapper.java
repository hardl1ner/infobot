package main.java.yaroslav.khahyda.address.model.image;

public class ImageWrapper {
    private Image[] images;

    public ImageWrapper() {
    }

    public Image[] getImages() {
        return images;
    }

    public void setImages(Image[] images) {
        this.images = images;
    }
}
