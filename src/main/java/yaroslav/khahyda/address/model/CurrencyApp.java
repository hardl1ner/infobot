package main.java.yaroslav.khahyda.address.model;

import javafx.beans.property.*;

/**
 * Класс-модель для курсу (CurrencyApp).
 *
 * @author Yaroslav Khakhyda
 */
public class CurrencyApp {
    //"https://finance.ua/ru/currency"
    private final StringProperty projectName;
    private final StringProperty stationName;
    private final StringProperty url;
    private final StringProperty updateCurrency;

    private final LongProperty idCashBuyUSD;
    private final LongProperty idCashSaleUSD;
    private final LongProperty idCashBuyEUR;
    private final LongProperty idCashSaleEUR;
    private final LongProperty idCashBuyRU;
    private final LongProperty idCashSaleRU;

    private final LongProperty idNbuUSD;
    private final LongProperty idNbuEUR;
    private final LongProperty idNbuRU;
    private final LongProperty idNbuPLN;


    private final IntegerProperty timeUpdate;

    public CurrencyApp (){
        this(null, null);
    }

    public CurrencyApp(String projectName, String stationName){
        this.projectName = new SimpleStringProperty(projectName);
        this.stationName = new SimpleStringProperty(stationName);

        // Какие-то фиктивные начальные данные для удобства тестирования.
        this.url = new SimpleStringProperty("http://wwww.com");
        this.updateCurrency = new SimpleStringProperty("nbu");

        this.idCashBuyUSD = new SimpleLongProperty(0);
        this.idCashSaleUSD = new SimpleLongProperty(0);
        this.idCashBuyEUR = new SimpleLongProperty(0);
        this.idCashSaleEUR = new SimpleLongProperty(0);
        this.idCashBuyRU = new SimpleLongProperty(0);
        this.idCashSaleRU = new SimpleLongProperty(0);

        this.idNbuUSD = new SimpleLongProperty(0);
        this.idNbuEUR= new SimpleLongProperty(0);
        this.idNbuRU = new SimpleLongProperty(0);
        this.idNbuPLN = new SimpleLongProperty(0);

        this.timeUpdate = new SimpleIntegerProperty(60);

    }

    public String getProjectName() {
        return projectName.get();
    }

    public StringProperty projectNameProperty() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName.set(projectName);
    }

    public String getStationName() {
        return stationName.get();
    }

    public StringProperty stationNameProperty() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName.set(stationName);
    }

    public String getUrl() {
        return url.get();
    }

    public StringProperty urlProperty() {
        return url;
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public String getUpdateCurrency() {
        return updateCurrency.get();
    }

    public void setUpdateCurrency(String updateCurrency) {
        this.updateCurrency.set(updateCurrency);
    }

    public StringProperty updateCurrencyProperty() {
        return updateCurrency;
    }

    public long getIdCashBuyUSD() {
        return idCashBuyUSD.get();
    }

    public void setIdCashBuyUSD(long idCashBuyUSD) {
        this.idCashBuyUSD.set(idCashBuyUSD);
    }

    public LongProperty idCashBuyUSDProperty() {
        return idCashBuyUSD;
    }

    public long getIdCashSaleUSD() {
        return idCashSaleUSD.get();
    }

    public void setIdCashSaleUSD(long idCashSaleUSD) {
        this.idCashSaleUSD.set(idCashSaleUSD);
    }

    public LongProperty idCashSaleUSDProperty() {
        return idCashSaleUSD;
    }

    public long getIdCashBuyEUR() {
        return idCashBuyEUR.get();
    }

    public void setIdCashBuyEUR(long idCashBuyEUR) {
        this.idCashBuyEUR.set(idCashBuyEUR);
    }

    public LongProperty idCashBuyEURProperty() {
        return idCashBuyEUR;
    }

    public long getIdCashSaleEUR() {
        return idCashSaleEUR.get();
    }

    public void setIdCashSaleEUR(long idCashSaleEUR) {
        this.idCashSaleEUR.set(idCashSaleEUR);
    }

    public LongProperty idCashSaleEURProperty() {
        return idCashSaleEUR;
    }

    public long getIdCashBuyRU() {
        return idCashBuyRU.get();
    }

    public void setIdCashBuyRU(long idCashBuyRU) {
        this.idCashBuyRU.set(idCashBuyRU);
    }

    public LongProperty idCashBuyRUProperty() {
        return idCashBuyRU;
    }

    public long getIdCashSaleRU() {
        return idCashSaleRU.get();
    }

    public void setIdCashSaleRU(long idCashSaleRU) {
        this.idCashSaleRU.set(idCashSaleRU);
    }

    public LongProperty idCashSaleRUProperty() {
        return idCashSaleRU;
    }

    public long getIdNbuUSD() {
        return idNbuUSD.get();
    }

    public void setIdNbuUSD(long idNbuUSD) {
        this.idNbuUSD.set(idNbuUSD);
    }

    public LongProperty idNbuUSDProperty() {
        return idNbuUSD;
    }

    public long getIdNbuEUR() {
        return idNbuEUR.get();
    }

    public void setIdNbuEUR(long idNbuEUR) {
        this.idNbuEUR.set(idNbuEUR);
    }

    public LongProperty idNbuEURProperty() {
        return idNbuEUR;
    }

    public long getIdNbuRU() {
        return idNbuRU.get();
    }

    public void setIdNbuRU(long idNbuRU) {
        this.idNbuRU.set(idNbuRU);
    }

    public LongProperty idNbuRUProperty() {
        return idNbuRU;
    }

    public long getIdNbuPLN() {
        return idNbuPLN.get();
    }

    public void setIdNbuPLN(long idNbuPLN) {
        this.idNbuPLN.set(idNbuPLN);
    }

    public LongProperty idNbuPLNProperty() {
        return idNbuPLN;
    }

    public int getTimeUpdate() {
        return timeUpdate.get();
    }

    public IntegerProperty timeUpdateProperty() {
        return timeUpdate;
    }

    public void setTimeUpdate(int timeUpdate) {
        this.timeUpdate.set(timeUpdate);
    }
}
