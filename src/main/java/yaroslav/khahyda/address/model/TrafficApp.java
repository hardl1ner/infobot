package main.java.yaroslav.khahyda.address.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 * Класс-модель для пробок (TrafficApp).
 *
 * @author Yaroslav Khakhyda
 */
public class TrafficApp {
    private final StringProperty projectName;
    private final StringProperty stationName;
    private final StringProperty filePath;
    private final StringProperty url;
    private final StringProperty blockTrafficImg;
    private final IntegerProperty timeUpdate;
    private final StringProperty dictionary;

    /**
     * Конструктор по умолчанию.
     */

    public TrafficApp (){
        this(null, null);
    }

    public TrafficApp(String projectName, String stationName){
        this.projectName = new SimpleStringProperty(projectName);
        this.stationName = new SimpleStringProperty(stationName);

        // Какие-то фиктивные начальные данные для удобства тестирования.
        this.filePath = new SimpleStringProperty("c:/test");
        this.url = new SimpleStringProperty("http:/www.com");
        this.blockTrafficImg = new SimpleStringProperty("01");
        this.timeUpdate = new SimpleIntegerProperty(60);
        this.dictionary = new SimpleStringProperty("WOG_dictionary_traffic");

    }

    public String getProjectName() {
        return projectName.get();
    }

    public void setProjectName(String projectName) {
        this.projectName.set(projectName);
    }

    public StringProperty projectNameProperty() {
        return projectName;
    }

    public String getStationName() {
        return stationName.get();
    }

    public void setStationName(String stationName) {
        this.stationName.set(stationName);
    }

    public StringProperty stationNameProperty() {
        return stationName;
    }

    public String getFilePath() {
        return filePath.get();
    }

    public void setFilePath(String filePath) {
        this.filePath.set(filePath);
    }

    public StringProperty filePathProperty() {
        return filePath;
    }

    public String getUrl() {
        return url.get();
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public StringProperty urlProperty() {
        return url;
    }

    public String getBlockTrafficImg() {
        return blockTrafficImg.get();
    }

    public void setBlockTrafficImg(String blockTrafficImg) {
        this.blockTrafficImg.set(blockTrafficImg);
    }

    public StringProperty blockTrafficImgProperty() {
        return blockTrafficImg;
    }

    public int getTimeUpdate() {
        return timeUpdate.get();
    }

    public void setTimeUpdate(int timeUpdate) {
        this.timeUpdate.set(timeUpdate);
    }

    public IntegerProperty timeUpdateProperty() {
        return timeUpdate;
    }

    public String getDictionary() {
        return dictionary.get();
    }

    public void setDictionary(String dictionary) {
        this.dictionary.set(dictionary);
    }

    public StringProperty dictionaryProperty() {
        return dictionary;
    }
}
