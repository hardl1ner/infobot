package main.java.yaroslav.khahyda.address.model;

import javafx.beans.property.*;

public class IQAirApp {
    private final StringProperty projectName;
    private final StringProperty stationName;
    private final StringProperty url;
    private final StringProperty address;
    private final LongProperty idText;
    private final LongProperty idImage;
    private final LongProperty idStationName;
    private final StringProperty filePath;

    private final IntegerProperty timeUpdate;

    /**
     * Конструктор по умолчанию.
     */

    public IQAirApp (){
        this(null, null);
    }

    public IQAirApp(String projectName, String stationName){
        this.projectName = new SimpleStringProperty(projectName);
        this.stationName = new SimpleStringProperty(stationName);
        this.idStationName = new SimpleLongProperty(0);
        this.url = new SimpleStringProperty("www.com");
        this.address = new SimpleStringProperty("Kyiv");
        this.idText = new SimpleLongProperty(0);
        this.idImage = new SimpleLongProperty(0);
        this.timeUpdate = new SimpleIntegerProperty(30);
        this.filePath = new SimpleStringProperty("c:/test");
    }

    public String getProjectName() {
        return projectName.get();
    }

    public StringProperty projectNameProperty() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName.set(projectName);
    }

    public String getStationName() {
        return stationName.get();
    }

    public StringProperty stationNameProperty() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName.set(stationName);
    }

    public String getUrl() {
        return url.get();
    }

    public StringProperty urlProperty() {
        return url;
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public String getAddress() {
        return address.get();
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public StringProperty addressProperty() {
        return address;
    }

    public long getIdText() {
        return idText.get();
    }

    public void setIdText(long idText) {
        this.idText.set(idText);
    }

    public LongProperty idTextProperty() {
        return idText;
    }

    public long getIdImage() {
        return idImage.get();
    }

    public void setIdImage(long idImage) {
        this.idImage.set(idImage);
    }

    public LongProperty idImageProperty() {
        return idImage;
    }

    public long getIdStationName() {
        return idStationName.get();
    }

    public void setIdStationName(long idStationName) {
        this.idStationName.set(idStationName);
    }

    public LongProperty idStationNameProperty() {
        return idStationName;
    }

    public String getFilePath() {
        return filePath.get();
    }

    public void setFilePath(String filePath) {
        this.filePath.set(filePath);
    }

    public StringProperty filePathProperty() {
        return filePath;
    }

    public int getTimeUpdate() {
        return timeUpdate.get();
    }

    public IntegerProperty timeUpdateProperty() {
        return timeUpdate;
    }

    public void setTimeUpdate(int timeUpdate) {
        this.timeUpdate.set(timeUpdate);
    }
}
