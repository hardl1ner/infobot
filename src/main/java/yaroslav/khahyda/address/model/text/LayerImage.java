package main.java.yaroslav.khahyda.address.model.text;

import lombok.Data;

@Data
public class LayerImage{
    private long id;
    private String name;
    private FileInfo fileInfo;
    private Integer width;
    private Integer height;
    private Integer x;
    private Integer y;
    private boolean active;

}
