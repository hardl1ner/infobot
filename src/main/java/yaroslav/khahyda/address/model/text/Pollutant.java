package main.java.yaroslav.khahyda.address.model.text;

public class Pollutant {
    private String pol;
    private String unit;
    private String time;
    private String value;
    private String averaging;

    public Pollutant() {
    }

    public String getPol() {

        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAveraging() {
        return averaging;
    }

    public void setAveraging(String averaging) {
        this.averaging = averaging;
    }
}
