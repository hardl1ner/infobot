package main.java.yaroslav.khahyda.address.model.text;

import lombok.Data;

import java.time.LocalDate;

@Data
public class FileInfo{
    private long id;
    private String file_name;
    private Long file_time;
    private Long file_size;
    private Integer file_width;
    private Integer file_height;
    private String file_type;
    private String file_path;
    private LocalDate uploadDate;
}
