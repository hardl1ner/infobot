package main.java.yaroslav.khahyda.address.model.text;

import lombok.Data;

@Data
public class LayerText {
    private long id;
    private String name;
    private Integer width;
    private Integer height;
    private Integer x;
    private Integer y;
    private Integer sizeFont;
    private String text;
    private String nameFont;
    private String styleFont;
    private String orientation;
    private String color;
    private String textAlignment;
    private String localeLanguage;

    private boolean active;
    private String type;

}
