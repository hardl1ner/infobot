package main.java.yaroslav.khahyda.address.model;

import javafx.beans.property.*;


/**
 * Класс-модель для погоды (WeatherApp).
 *
 * @author Yaroslav Khakhyda
 */
public class WeatherApp {
    private final StringProperty projectName;
    private final StringProperty stationName;
    private final StringProperty url;

    private final LongProperty idToday;
    private final LongProperty idTodayImg;
    private final StringProperty blockTodayFormat;

    private final LongProperty idTomorrow;
    private final LongProperty idTomorrowImg;
    private final StringProperty blockTomorrowFormat;

    private final IntegerProperty timeUpdate;
    private final StringProperty dictionary;
    private final StringProperty dayUpdate;

    /**
     * Конструктор по умолчанию.
     */

    public WeatherApp(){
        this(null, null);
    }
    /**
     * Конструктор с некоторыми начальными данными.
     *
     */

    public WeatherApp(String projectName, String stationName){
        this.projectName = new SimpleStringProperty(projectName);
        this.stationName = new SimpleStringProperty(stationName);

        // Какие-то фиктивные начальные данные для удобства тестирования.
        this.url = new SimpleStringProperty("http:/www.com");
        this.idToday = new SimpleLongProperty(0);
        this.idTodayImg = new SimpleLongProperty(0);
        this.blockTodayFormat = new SimpleStringProperty("...");
        this.idTomorrow = new SimpleLongProperty(0);
        this.idTomorrowImg = new SimpleLongProperty(0);
        this.blockTomorrowFormat = new SimpleStringProperty("...");
        this.timeUpdate = new SimpleIntegerProperty(60);
        this.dictionary = new SimpleStringProperty("WOG_dictionary");
        this.dayUpdate = new SimpleStringProperty("today");

    }

    public String getProjectName() {
        return projectName.get();
    }

    public StringProperty projectNameProperty() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName.set(projectName);
    }

    public String getStationName() {
        return stationName.get();
    }

    public StringProperty stationNameProperty() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName.set(stationName);
    }

    public String getUrl() {
        return url.get();
    }

    public StringProperty urlProperty() {
        return url;
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public long getIdToday() {
        return idToday.get();
    }

    public void setIdToday(long idToday) {
        this.idToday.set(idToday);
    }

    public LongProperty idTodayProperty() {
        return idToday;
    }

    public long getIdTodayImg() {
        return idTodayImg.get();
    }

    public void setIdTodayImg(long idTodayImg) {
        this.idTodayImg.set(idTodayImg);
    }

    public LongProperty idTodayImgProperty() {
        return idTodayImg;
    }

    public String getBlockTodayFormat() {
        return blockTodayFormat.get();
    }

    public StringProperty blockTodayFormatProperty() {
        return blockTodayFormat;
    }

    public void setBlockTodayFormat(String blockTodayFormat) {
        this.blockTodayFormat.set(blockTodayFormat);
    }

    public long getIdTomorrow() {
        return idTomorrow.get();
    }

    public void setIdTomorrow(long idTomorrow) {
        this.idTomorrow.set(idTomorrow);
    }

    public LongProperty idTomorrowProperty() {
        return idTomorrow;
    }

    public long getIdTomorrowImg() {
        return idTomorrowImg.get();
    }

    public void setIdTomorrowImg(long idTomorrowImg) {
        this.idTomorrowImg.set(idTomorrowImg);
    }

    public LongProperty idTomorrowImgProperty() {
        return idTomorrowImg;
    }

    public String getBlockTomorrowFormat() {
        return blockTomorrowFormat.get();
    }

    public StringProperty blockTomorrowFormatProperty() {
        return blockTomorrowFormat;
    }

    public void setBlockTomorrowFormat(String blockTomorrowFormat) {
        this.blockTomorrowFormat.set(blockTomorrowFormat);
    }

    public int getTimeUpdate() {
        return timeUpdate.get();
    }

    public IntegerProperty timeUpdateProperty() {
        return timeUpdate;
    }

    public void setTimeUpdate(int timeUpdate) {
        this.timeUpdate.set(timeUpdate);
    }

    public String getDictionary() {
        return dictionary.get();
    }

    public StringProperty dictionaryProperty() {
        return dictionary;
    }

    public void setDictionary(String dictionary) {
        this.dictionary.set(dictionary);
    }

    public String getDayUpdate() {
        return dayUpdate.get();
    }

    public StringProperty dayUpdateProperty() {
        return dayUpdate;
    }

    public void setDayUpdate(String dayUpdate) {
        this.dayUpdate.set(dayUpdate);
    }
}
