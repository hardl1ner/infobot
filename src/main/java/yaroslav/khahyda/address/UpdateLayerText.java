package main.java.yaroslav.khahyda.address;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import main.java.yaroslav.khahyda.address.conf.AppConf;
import main.java.yaroslav.khahyda.address.model.text.LayerText;
import org.apache.commons.codec.binary.Base64;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

public class UpdateLayerText {
    private AppConf appConf;
    public UpdateLayerText(){
        appConf = new AppConf();
    }
     LayerText getLayerText(long id){
        HttpURLConnection connection = null;
        InputStream content = null;
        BufferedReader br = null;
        LayerText layerText = null;
        try {
            URL url = new URL(this.appConf.getUrl() + "api/info/text/" + id);
            Base64 b = new Base64();
            String encoding = b.encodeAsString((this.appConf.getUsername() + ":" + this.appConf.getPassword()).getBytes());
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            content = connection.getInputStream();
            br = new BufferedReader(new InputStreamReader(content, StandardCharsets.UTF_8));
            ObjectMapper mapper = new ObjectMapper();
            String line = "";
            String temp = "";
            while ((line = br.readLine()) != null) {
                temp = temp + line;
            }
            mapper.registerModule(new JavaTimeModule());
            mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            layerText = mapper.readValue(temp, LayerText.class);

            return layerText;
        }
        catch (UnknownHostException e) {
            System.out.println("" + e);
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error");
                alert.setContentText("ERROR. There is no connection with the server. Check your internet access");

                alert.showAndWait();
            });
            return layerText;
        } catch (IOException e) {
            System.out.println("" + e);
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error");
                alert.setContentText("ERROR " + e);

                alert.showAndWait();
            });

            return layerText;

        } finally {
            if (connection != null){
                connection.disconnect();
            }
            try {
                if (content != null) {
                    content.close();
                }
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                System.out.println("" + e);
                Platform.runLater(() -> {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error");
                    alert.setContentText("ERROR " + e);

                    alert.showAndWait();
                });


            }

        }

    }

     boolean updateText(LayerText layerText, long id){
        HttpURLConnection connection = null;
        InputStream content = null;
        BufferedReader br = null;
        try {
            URL url = new URL(this.appConf.getUrl() + "api/info/text/" + id);
            Base64 b = new Base64();
            String encoding = b.encodeAsString((this.appConf.getUsername() + ":" + this.appConf.getPassword()).getBytes());
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("PUT");
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            connection.setRequestProperty("Content-Type", "application/json");

            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            String text = mapper.writeValueAsString(layerText);

            byte[] out = text.getBytes(StandardCharsets.UTF_8);

            OutputStream stream = connection.getOutputStream();
            stream.write(out);

            System.out.println(connection.getResponseCode() + " " + connection.getResponseMessage());
            connection.disconnect();


            return true;
        }
        catch (UnknownHostException e) {
            System.out.println("" + e);
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error Update LayerText");
                alert.setContentText("ERROR. There is no connection with the server. Check your internet access");

                alert.showAndWait();
            });
            return false;
        } catch (IOException e) {
            System.out.println("" + e);
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error Update LayerText");
                alert.setContentText("ERROR " + e);

                alert.showAndWait();
            });

            return false;

        } finally {
            if (connection != null){
                connection.disconnect();
            }
            try {
                if (content != null) {
                    content.close();
                }
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                System.out.println("" + e);
                Platform.runLater(() -> {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error Update LayerText");
                    alert.setContentText("ERROR " + e);

                    alert.showAndWait();
                });


            }

        }

    }
}
