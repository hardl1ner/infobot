package main.java.yaroslav.khahyda.address.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.java.yaroslav.khahyda.address.model.IQAirApp;

import java.io.File;

public class IQAirAppEditDialogController {

    @FXML
    private TextField projectNameField;

    @FXML
    private TextField stationNameField;

    @FXML
    private TextField urlField;

    @FXML
    private TextField timeUpdateField;

    @FXML
    private TextField idStationNameField;

    @FXML
    private TextField filePatchField;

    @FXML
    private TextField adressField;

    @FXML
    private TextField idTextIQairField;

    @FXML
    private TextField idImgIQAirField;

    private Stage dialogIQAirStage;
    private IQAirApp iqAirApp;
    private boolean okClicked = false;

    public Stage getDialogIQAirStage() {
        return dialogIQAirStage;
    }

    public void setDialogIQAirStage(Stage dialogIQAirStage) {
        this.dialogIQAirStage = dialogIQAirStage;
    }

    public void setIQAirApp(IQAirApp iqAirApp){
        this.iqAirApp = iqAirApp;

        projectNameField.setText(iqAirApp.getProjectName());
        idStationNameField.setText(String.valueOf(iqAirApp.getIdStationName()));
        stationNameField.setText(iqAirApp.getStationName());
        urlField.setText(iqAirApp.getUrl());
        adressField.setText(iqAirApp.getAddress());
        idTextIQairField.setText(String.valueOf(iqAirApp.getIdText()));
        idImgIQAirField.setText(String.valueOf(iqAirApp.getIdImage()));
        timeUpdateField.setText(Integer.toString(iqAirApp.getTimeUpdate()));
        filePatchField.setText(iqAirApp.getFilePath());

    }

    public boolean isOkClicked() {
        return okClicked;
    }


    @FXML
    void handleCancel() {
        dialogIQAirStage.close();
    }

    @FXML
    void handleOk() {
        if (isInputValid()) {

            iqAirApp.setProjectName(projectNameField.getText());
            iqAirApp.setIdStationName(Long.parseLong(idStationNameField.getText()));
            iqAirApp.setStationName(stationNameField.getText());
            iqAirApp.setUrl(urlField.getText());
            iqAirApp.setAddress(adressField.getText());
            iqAirApp.setIdText(Long.parseLong(idTextIQairField.getText()));
            iqAirApp.setIdImage(Long.parseLong(idImgIQAirField.getText()));
            iqAirApp.setTimeUpdate(Integer.parseInt(timeUpdateField.getText()));
            iqAirApp.setFilePath(filePatchField.getText());



            okClicked = true;
            dialogIQAirStage.close();
        }
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if (projectNameField.getText() == null || projectNameField.getText().length() == 0) {
            errorMessage += "Нет введенного имени проэкта!\n";
        }
        if (filePatchField.getText() == null || filePatchField.getText().length() == 0) {
            errorMessage += "Выберите путь к словарю!\n";
        }
        if (stationNameField.getText() == null || stationNameField.getText().length() == 0) {
            errorMessage += "Нет введенного имени станции!!\n";
        }
        if (idStationNameField.getText() == null || idStationNameField.getText().length() == 0) {
            errorMessage += "Не казанна станция качества воздуха!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(idStationNameField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id station (должно быть целое число)!\n";
            }
        }
        if (urlField.getText() == null || urlField.getText().length() == 0) {
            errorMessage += "Не введен сайт!!\n";
        }

        if (adressField.getText() == null || adressField.getText().length() == 0) {
            errorMessage += "Не введено адресс!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Integer.parseInt(timeUpdateField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное время (должно быть целое число)!\n";
            }
        }

        if (idTextIQairField.getText() == null || idTextIQairField.getText().length() == 0) {
            errorMessage += "Не введен блок текста!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(idTextIQairField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id text (должно быть целое число)!\n";
            }
        }
        if (idImgIQAirField.getText() == null || idImgIQAirField.getText().length() == 0) {
            errorMessage += "Не введен блок картинки!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(idImgIQAirField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id img (должно быть целое число)!\n";
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Показываем сообщение об ошибке.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogIQAirStage);
            alert.setTitle("Неверные поля курсу");
            alert.setHeaderText("Пожалуйста, заполните правильно поля");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    @FXML
    private void handleBrowser() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Open File txt");
        // Задаём фильтр расширений
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "TXT files (*.txt)", "*.txt");
        chooser.getExtensionFilters().add(extFilter);
        File file = chooser.showOpenDialog(new Stage());
        filePatchField.setText(file.getAbsolutePath());
    }

    @FXML
    void initialize() {


    }
}
