package main.java.yaroslav.khahyda.address.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.java.yaroslav.khahyda.address.model.CurrencyApp;

/**
 * Окно для изменения информации об курсе.
 *
 * @author Marco Jakob
 */

public class CurrencyAppEditDialogController {
    @FXML
    private TextField nameProjectField;
    @FXML
    private TextField nameStationField;
    @FXML
    private TextField urlField;
    @FXML
    private ComboBox<String> nbuComboBox;
    @FXML
    private TextField timeUpdatetField;


    @FXML
    private TextField cashBuyUSDField;
    @FXML
    private TextField cashSaleUSDField;
    @FXML
    private TextField cashBuyEURField;
    @FXML
    private TextField cashSaleEURField;
    @FXML
    private TextField cashBuyRUField;
    @FXML
    private TextField cashSaleRUField;


    @FXML
    private TextField nbuUSDField;
    @FXML
    private TextField nbuEURField;
    @FXML
    private TextField nbuRUField;
    @FXML
    private TextField nbuPLNField;

    private Stage dialogCurrencyStage;
    private CurrencyApp currencyApp;
    private boolean okClicked = false;

    /**
     * Инициализирует класс-контроллер. Этот метод вызывается автоматически
     * после того, как fxml-файл будет загружен.
     */
    @FXML
    private void initialize() {
        ObservableList<String> langs = FXCollections.observableArrayList("nbu", "cash");
        nbuComboBox.setItems(langs);
    }

    /**
     * Устанавливает сцену для этого окна.
     *
     * @param dialogCurrencyStage
     */
    public void setDialogCurrencyStage(Stage dialogCurrencyStage) {
        this.dialogCurrencyStage = dialogCurrencyStage;
    }
    /**
     * Задаёт курс, информацию о которой будем менять.
     *
     * @param currencyApp
     */
    public void setCurrencyApp(CurrencyApp currencyApp) {
        this.currencyApp = currencyApp;

        nameProjectField.setText(currencyApp.getProjectName());
        nameStationField.setText(currencyApp.getStationName());
        urlField.setText(currencyApp.getUrl());
        nbuComboBox.setValue(currencyApp.getUpdateCurrency());
        timeUpdatetField.setText(Integer.toString(currencyApp.getTimeUpdate()));

        cashBuyUSDField.setText(Long.toString(currencyApp.getIdCashBuyUSD()));
        cashSaleUSDField.setText(Long.toString(currencyApp.getIdCashSaleUSD()));
        cashBuyEURField.setText(Long.toString(currencyApp.getIdCashBuyEUR()));
        cashSaleEURField.setText(Long.toString(currencyApp.getIdCashSaleEUR()));
        cashBuyRUField.setText(Long.toString(currencyApp.getIdCashBuyRU()));
        cashSaleRUField.setText(Long.toString(currencyApp.getIdCashSaleRU()));

        nbuUSDField.setText(Long.toString(currencyApp.getIdNbuUSD()));
        nbuEURField.setText(Long.toString(currencyApp.getIdNbuEUR()));
        nbuRUField.setText(Long.toString(currencyApp.getIdNbuRU()));
        nbuPLNField.setText(Long.toString(currencyApp.getIdNbuPLN()));
    }

    /**
     * Returns true, если пользователь кликнул OK, в другом случае false.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Вызывается, когда пользователь кликнул по кнопке OK.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            currencyApp.setProjectName(nameProjectField.getText());
            currencyApp.setStationName(nameStationField.getText());
            currencyApp.setUrl(urlField.getText());
            currencyApp.setUpdateCurrency(nbuComboBox.getValue());
            currencyApp.setTimeUpdate(Integer.parseInt(timeUpdatetField.getText()));

            currencyApp.setIdCashBuyUSD(Long.parseLong(cashBuyUSDField.getText()));
            currencyApp.setIdCashSaleUSD(Long.parseLong(cashSaleUSDField.getText()));
            currencyApp.setIdCashBuyEUR(Long.parseLong(cashBuyEURField.getText()));
            currencyApp.setIdCashSaleEUR(Long.parseLong(cashBuyEURField.getText()));
            currencyApp.setIdCashBuyRU(Long.parseLong(cashBuyRUField.getText()));
            currencyApp.setIdCashSaleRU(Long.parseLong(cashSaleRUField.getText()));

            currencyApp.setIdNbuUSD(Long.parseLong(nbuUSDField.getText()));
            currencyApp.setIdNbuEUR(Long.parseLong(nbuEURField.getText()));
            currencyApp.setIdNbuRU(Long.parseLong(nbuRUField.getText()));
            currencyApp.setIdNbuPLN(Long.parseLong(nbuPLNField.getText()));

            okClicked = true;
            dialogCurrencyStage.close();
        }
    }

    /**
     * Вызывается, когда пользователь кликнул по кнопке Cancel.
     */
    @FXML
    private void handleCancel() {
        dialogCurrencyStage.close();
    }

    /**
     * Проверяет пользовательский ввод в текстовых полях.
     *
     * @return true, если пользовательский ввод корректен
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (nameProjectField.getText() == null || nameProjectField.getText().length() == 0) {
            errorMessage += "Нет введенного имени проэкта!\n";
        }
        if (nameStationField.getText() == null || nameStationField.getText().length() == 0) {
            errorMessage += "Нет введенного имени станции!!\n";
        }

        if (urlField.getText() == null || urlField.getText().length() == 0) {
            errorMessage += "Не введен сайт!!\n";
        }
        if (nbuComboBox.getValue() == null || nbuComboBox.getValue().length() == 0) {
            errorMessage += "Не введен сайт!!\n";
        }

        if (timeUpdatetField.getText() == null || timeUpdatetField.getText().length() == 0) {
            errorMessage += "Не введено время обновления!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Integer.parseInt(timeUpdatetField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное время (должно быть целое число)!\n";
            }
        }

        if (cashBuyUSDField.getText() == null || cashBuyUSDField.getText().length() == 0) {
            errorMessage += "Не введен блок курса покупки USD!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(cashBuyUSDField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id usd покупка (должно быть целое число)!\n";
            }
        }
        if (cashSaleUSDField.getText() == null || cashSaleUSDField.getText().length() == 0) {
            errorMessage += "Не введен блок курса продажи USD!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(cashSaleUSDField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id usd продажа (должно быть целое число)!\n";
            }
        }
        if (cashBuyEURField.getText() == null || cashBuyEURField.getText().length() == 0) {
            errorMessage += "Не введен блок курса покупки EUR!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(cashBuyEURField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id eur покупка (должно быть целое число)!\n";
            }
        }
        if (cashSaleEURField.getText() == null || cashSaleEURField.getText().length() == 0) {
            errorMessage += "Не введен блок курса продажи EUR!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(cashSaleEURField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id eur продажа (должно быть целое число)!\n";
            }
        }
        if (cashBuyRUField.getText() == null || cashBuyRUField.getText().length() == 0) {
            errorMessage += "Не введен блок курса покупки RU!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(cashBuyRUField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id ru покупка (должно быть целое число)!\n";
            }
        }
        if (cashSaleRUField.getText() == null || cashSaleRUField.getText().length() == 0) {
            errorMessage += "Не введен блок курса продажи RU!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(cashSaleRUField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id ru продажа (должно быть целое число)!\n";
            }
        }

        if (nbuUSDField.getText() == null || nbuUSDField.getText().length() == 0) {
            errorMessage += "Не введен блок курса НБУ USD!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(nbuUSDField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id usd (должно быть целое число)!\n";
            }
        }
        if (nbuEURField.getText() == null || nbuEURField.getText().length() == 0) {
            errorMessage += "Не введен блок курса НБУ EUR!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(nbuEURField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id eur (должно быть целое число)!\n";
            }
        }
        if (nbuRUField.getText() == null || nbuRUField.getText().length() == 0) {
            errorMessage += "Не введен блок курса НБУ RU!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(nbuRUField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id ru (должно быть целое число)!\n";
            }
        }

        if (nbuPLNField.getText() == null || nbuPLNField.getText().length() == 0) {
            errorMessage += "Не введен блок курса НБУ PLN!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Long.parseLong(nbuPLNField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id pln (должно быть целое число)!\n";
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Показываем сообщение об ошибке.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogCurrencyStage);
            alert.setTitle("Неверные поля курсу");
            alert.setHeaderText("Пожалуйста, заполните правильно поля");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }



}
