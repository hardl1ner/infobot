package main.java.yaroslav.khahyda.address.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import main.java.yaroslav.khahyda.address.CheckInternetConnection;
import main.java.yaroslav.khahyda.address.MainApp;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Контроллер для корневого макета. Корневой макет предоставляет базовый
 * макет приложения, содержащий строку меню и место, где будут размещены
 * остальные элементы JavaFX.
 *
 * @author Yaroslav Khakhyda
 */
public class RootLayoutController {
    // Ссылка на главное приложение
    List<Thread> threads = new ArrayList<>();
    private MainApp mainApp;

    public boolean isStartUpdateWeather() {
        return startUpdateWeather;
    }

    public void setStartUpdateWeather(boolean startUpdateWeather) {
        this.startUpdateWeather = startUpdateWeather;
    }

    public boolean isStartUpdateTraffic() {
        return startUpdateTraffic;
    }

    public void setStartUpdateTraffic(boolean startUpdateTraffic) {
        this.startUpdateTraffic = startUpdateTraffic;
    }

    public boolean isStartUpdateCurrency() {
        return startUpdateCurrency;
    }

    public void setStartUpdateCurrency(boolean startUpdateCurrency) {
        this.startUpdateCurrency = startUpdateCurrency;
    }

    private boolean startUpdateIqAir = true;

    public boolean isStartUpdateIqAir() {
        return startUpdateIqAir;
    }

    private boolean startUpdateWeather = true;
    private boolean startUpdateTraffic = true;
    private boolean startUpdateCurrency = true;

    public void setStartUpdateIqAir(boolean startUpdateIqAir) {
        this.startUpdateIqAir = startUpdateIqAir;
    }



    /**
     * Вызывается главным приложением, чтобы оставить ссылку на самого себя.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    /**
     * Создаёт пустые настройки погоды.
     */
    @FXML
    private void handleNew() {
        mainApp.getWeatherAppData().clear();
        mainApp.getTrafficAppData().clear();
        mainApp.getCurrencyAppData().clear();
        mainApp.getIqAirAppsData().clear();
        mainApp.setInfoBotFilePath(null);
    }

    /**
     * Открывает FileChooser, чтобы пользователь имел возможность
     * выбрать настройки погоды для загрузки.
     */
    @FXML
    private void handleOpen() {
        FileChooser fileChooser = new FileChooser();

        // Задаём фильтр расширений
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        // Показываем диалог загрузки файла
        File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());

        if (file != null) {
            mainApp.loadInfoBotFromFile(file);
        }
    }
    @FXML
    private void initialize() {

    }

    /**
     * Сохраняет файл в файл настроек погоды, который в настоящее время открыт.
     * Если файл не открыт, то отображается диалог "save as".
     */
    @FXML
    private void handleSave() {
        File personFile = mainApp.getInfoBotFilePath();
        if (personFile != null) {
            mainApp.saveInfoBotToFile(personFile);
        } else {
            handleSaveAs();
        }
    }

    /**
     * Открывает FileChooser, чтобы пользователь имел возможность
     * выбрать файл, куда будут сохранены данные
     */
    @FXML
    private void handleSaveAs() {
        FileChooser fileChooser = new FileChooser();

        // Задаём фильтр расширений
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        // Показываем диалог сохранения файла
        File file = fileChooser.showSaveDialog(mainApp.getPrimaryStage());

        if (file != null) {
            // Make sure it has the correct extension
            if (!file.getPath().endsWith(".xml")) {
                file = new File(file.getPath() + ".xml");
            }
            mainApp.saveInfoBotToFile(file);
        }
    }

    /**
     * Открывает диалоговое окно about.
     */
    @FXML
    private void handleAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("InfoBot");
        alert.setHeaderText("About");
        alert.setContentText("@Author: Yaroslav Khakhyda\nVersion: 3.18.0");

        alert.showAndWait();
    }

    /**
     * Закрывает приложение.
     */
    @FXML
    private void handleExit() {
        System.exit(0);
    }
    //запуск работы бота
    @FXML
    public void handleStart () {
        try {
            setStartUpdateWeather(true);
            setStartUpdateCurrency(true);
            setStartUpdateTraffic(true);
            setStartUpdateIqAir(true);


            final int[] timeUpdate = {0};
            final int[] timeUpdateTraffic = {0};
            final int[] timeUpdateCurrency ={0};
            final int[] timeUpdateIqAir = {0};
            Thread run = new Thread(() -> {
                while (startUpdateWeather) {
                    try {
                        int time;
                        CheckInternetConnection.setUrl("https://sinoptik.ua");
                        if(CheckInternetConnection.checkInternet(CheckInternetConnection.getUrl())) {
                            timeUpdate[0] = mainApp.updateWeather();
                            time = timeUpdate[0] * (60 * 1000);
                        } else {
                            mainApp.getBotOverviewController().setTerminalWeather(CheckInternetConnection.getStatus()+"\n");
                            time = 5 * (60 * 1000);
                        }

                        Thread.sleep(time); //1000 - 1 сек
                    } catch (InterruptedException ignored) {

                    }
                }
            });
            threads.add(run);
            Thread run2 = new Thread(() -> {
                while (startUpdateTraffic) {
                    try {
                        int time;
                        CheckInternetConnection.setUrl("https://yandex.ru");
                        if(CheckInternetConnection.checkInternet(CheckInternetConnection.getUrl())){
                            timeUpdateTraffic[0] = mainApp.updateTraffic();
                            time = timeUpdateTraffic[0] * (60 * 1000);
                        } else {
                            mainApp.getBotOverviewController().setTerminalTraffic(CheckInternetConnection.getStatus()+"\n");
                            time = 5 * (60 * 1000);

                        }
                        Thread.sleep(time); //1000 - 1 сек
                    } catch (InterruptedException ignored) {

                    }
                }
            });
            threads.add(run2);
            Thread run3 = new Thread(() -> {
                while (startUpdateCurrency) {
                    try {
                        int time;
                        CheckInternetConnection.setUrl("https://finance.ua");
                        if(CheckInternetConnection.checkInternet(CheckInternetConnection.getUrl())) {
                            timeUpdateCurrency[0] = mainApp.updateCurrency();
                            time = timeUpdateCurrency[0] * (60 * 1000);
                        } else {
                            mainApp.getBotOverviewController().setTerminalCurrency(CheckInternetConnection.getStatus()+"\n");
                            time = 5 * (60 * 1000);
                            mainApp.stopWebDriver();
                        }
                        Thread.sleep(time); //1000 - 1 сек
                    } catch (InterruptedException en) {
                        mainApp.stopWebDriver();
                    }
                }
            });
            threads.add(run3);
            Thread run4 = new Thread(() -> {
                while (startUpdateIqAir) {
                    try {
                        int time;
                        CheckInternetConnection.setUrl("https://www.iqair.com/");
                        if(CheckInternetConnection.checkInternet(CheckInternetConnection.getUrl())) {
                            timeUpdateIqAir[0] = mainApp.updateIqAir();
                            time = timeUpdateIqAir[0] * (60 * 1000);
                            System.out.println(startUpdateIqAir);
                        } else {
                            mainApp.getBotOverviewController().setTerminalIqair(CheckInternetConnection.getStatus()+"\n");
                            time = 5 * (60 * 1000);
                        }
                        Thread.sleep(time); //1000 - 1 сек
                    } catch (InterruptedException en) {
                        mainApp.stopWebDriver();
                    }
                }
            });
            threads.add(run4);
            if(startUpdateWeather) {
                mainApp.getBotOverviewController().setTerminalWeather("!START: Обновление погоды запущено!\n");
                run.start();

            }
            if (startUpdateTraffic){
                mainApp.getBotOverviewController().setTerminalTraffic("!START: Обновление пробок запущено!\n");
                run2.start();
            }

            if (startUpdateCurrency){
                mainApp.getBotOverviewController().setTerminalCurrency("!START: Обновление курса запущено!\n");
                run3.start();
            }
            if (startUpdateIqAir){
                mainApp.getBotOverviewController().setTerminalIqair("!START: Обновление Качество воздуха запущено!\n");
                run4.start();
            }

        } catch (Exception e) {
        System.err.format("IOException: %s%n", e);

        }





    }

    //остановка работы бота
    @FXML
    public void handleStop(){
        setStartUpdateWeather(false);
        setStartUpdateCurrency(false);
        setStartUpdateTraffic(false);
        setStartUpdateIqAir(false);

        for (Thread thread : threads){
            thread.interrupt();
        }
        mainApp.stopWebDriver();
        mainApp.getBotOverviewController().setTerminalWeather("!STOP: Обновление погоды остановлено!\n");
        mainApp.getBotOverviewController().setTerminalTraffic("!STOP: Обновление пробок остановлено!\n");
        mainApp.getBotOverviewController().setTerminalCurrency("!STOP: Обновление курса остановлено!\n");
        mainApp.getBotOverviewController().setTerminalIqair("!STOP: Обновление Качество воздуха остановлено!\n");



    }



}
