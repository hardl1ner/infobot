package main.java.yaroslav.khahyda.address.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import main.java.yaroslav.khahyda.address.MainApp;
import main.java.yaroslav.khahyda.address.model.CurrencyApp;
import main.java.yaroslav.khahyda.address.model.IQAirApp;
import main.java.yaroslav.khahyda.address.model.TrafficApp;
import main.java.yaroslav.khahyda.address.model.WeatherApp;
/**
 * Класс-модель для пробок (WeatherApp).
 *
 * @author Yaroslav Khakhyda
 */

import java.io.IOException;

public class BotOverviewController {
    @FXML
    private TableView<WeatherApp> weatherAppTable;
    @FXML
    private TableColumn<WeatherApp, String> projectNameColum;
    @FXML
    private TableColumn<WeatherApp, String> stationNameColum;
    @FXML
    public TextArea terminalWeather;

    //---------------------------------------------------------
    @FXML
    public TextArea terminalTraffic;
    @FXML
    private TableView<TrafficApp> trafficAppTable;
    @FXML
    private TableColumn<TrafficApp, String> projectNameColumTraffic;
    //--------------------------------------------------------
    @FXML
    public TextArea terminalCurrency;
    @FXML
    private TableColumn<TrafficApp, String> stationNameColumTraffic;
    @FXML
    private TableView<CurrencyApp> currencyAppTable;
    @FXML
    private TableColumn<CurrencyApp, String> projectNameColumCurrency;
    @FXML
    private TableColumn<CurrencyApp, String> stationtNameColumCurrency;

    // IQAir ----------------------------------------------------------------
    @FXML
    private TableView<IQAirApp> iqairAppTable;

    @FXML
    private TableColumn<IQAirApp, String> projectNameColumIqair;

    @FXML
    private TableColumn<IQAirApp, String> stationtNameColumIqair;

    @FXML
    private TextArea terminalIqair;


    // Ссылка на главное приложение.
    private MainApp mainApp;




    /**
     * Конструктор.
     * Конструктор вызывается раньше метода initialize().
     */

    public BotOverviewController() {

    }


/**
 * Инициализация класса-контроллера. Этот метод вызывается автоматически
 * после того, как fxml-файл будет загружен.
 */
    @FXML
    private void initialize() {
    // Инициализация таблицы погоды с двумя столбцами.
    projectNameColum.setCellValueFactory(cellData -> cellData.getValue().projectNameProperty());
    stationNameColum.setCellValueFactory(cellData -> cellData.getValue().stationNameProperty());

    // Инициализация таблицы пробок с двумя столбцами.
    projectNameColumTraffic.setCellValueFactory(cellData -> cellData.getValue().projectNameProperty());
    stationNameColumTraffic.setCellValueFactory(cellData -> cellData.getValue().stationNameProperty());

    // Инициализация таблицы курса с двумя столбцами.
    projectNameColumCurrency.setCellValueFactory(cellData -> cellData.getValue().projectNameProperty());
    stationtNameColumCurrency.setCellValueFactory(cellData -> cellData.getValue().stationNameProperty());
    stationtNameColumIqair.setCellValueFactory(cellData -> cellData.getValue().stationNameProperty());
    projectNameColumIqair.setCellValueFactory(cellData -> cellData.getValue().projectNameProperty());

    terminalWeather.setEditable(false);
    terminalTraffic.setEditable(false);
    terminalCurrency.setEditable(false);
    terminalIqair.setEditable(false);






    }
    /**
     * Вызывается главным приложением, которое даёт на себя ссылку.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;

        // Добавление в таблицу данных из наблюдаемого списка
        weatherAppTable.setItems(mainApp.getWeatherAppData());
        trafficAppTable.setItems(mainApp.getTrafficAppData());
        currencyAppTable.setItems(mainApp.getCurrencyAppData());
        iqairAppTable.setItems(mainApp.getIqAirAppsData());
    }

    /**
     * Вызывается, когда пользователь кликает по кнопке удаления.
     */
    @FXML
    void handleDeleteIqairApp() {
        int selectedIndex = iqairAppTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            iqairAppTable.getItems().remove(selectedIndex);
        } else {
            // Ничего не выбрано.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Ничего не выбрано");
            alert.setHeaderText("Нет выбраной настройки iqair для удаления");
            alert.setContentText("Пожалуйста, выберите настройки iqair в таблице, для удаления.");

            alert.showAndWait();
        }
    }
    @FXML
    private void handleDeleteWeatherApp() {
        int selectedIndex = weatherAppTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            weatherAppTable.getItems().remove(selectedIndex);
        } else {
            // Ничего не выбрано.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Ничего не выбрано");
            alert.setHeaderText("Нет выбраной настройки погоды для удаления");
            alert.setContentText("Пожалуйста, выберите настройки погоды в таблице, для удаления.");

            alert.showAndWait();
        }
    }
    @FXML
    private void handleDeleteTrafficApp() {
        int selectedIndex = trafficAppTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            trafficAppTable.getItems().remove(selectedIndex);
        } else {
            // Ничего не выбрано.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Ничего не выбрано");
            alert.setHeaderText("Нет выбраной настройки пробок для удаления");
            alert.setContentText("Пожалуйста, выберите настройки пробок в таблице, для удаления.");

            alert.showAndWait();
        }
    }
    /**
     * Вызывается, когда пользователь кликает по кнопке удаления.
     */
    @FXML
    private void handleDeleteCurrecyApp() {
        int selectedIndex = currencyAppTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            currencyAppTable.getItems().remove(selectedIndex);
        } else {
            // Ничего не выбрано.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Ничего не выбрано");
            alert.setHeaderText("Нет выбраной настройки курса для удаления");
            alert.setContentText("Пожалуйста, выберите настройки курса в таблице, для удаления.");

            alert.showAndWait();
        }
    }

    /**
     * Вызывается, когда пользователь кликает по кнопке New...
     * Открывает диалоговое окно с дополнительной информацией новой погоды.
     */

    @FXML
    void handleNewIqairApp() {
        IQAirApp iqAirApp = new IQAirApp();
        boolean okClicked = mainApp.showIqAirAppEditDialog(iqAirApp);
        if (okClicked) {
            mainApp.getIqAirAppsData().add(iqAirApp);
        }
    }

    @FXML
    private void handleNewWeatherApp() {
        WeatherApp tempWeatherApp = new WeatherApp();
        boolean okClicked = mainApp.showWeatherAppEditDialog(tempWeatherApp);
        if (okClicked) {
            mainApp.getWeatherAppData().add(tempWeatherApp);
        }
    }
    /**
     * Вызывается, когда пользователь кликает по кнопке New...
     * Открывает диалоговое окно с дополнительной информацией новой настройки пробок.
     */
    @FXML
    private void handleNewTrafficApp() {
        TrafficApp tempTrafficApp = new TrafficApp();
        boolean okClicked = mainApp.showTrafficAppEditDialog(tempTrafficApp);
        if (okClicked) {
            mainApp.getTrafficAppData().add(tempTrafficApp);
        }
    }

    /**
     * Вызывается, когда пользователь кликает по кнопке New...
     * Открывает диалоговое окно с дополнительной информацией новой настройки курса.
     */
    @FXML
    private void handleNewCurrencyApp() {
        CurrencyApp tempCurrencyApp= new CurrencyApp();
        boolean okClicked = mainApp.showCurrencyAppEditDialog(tempCurrencyApp);
        if (okClicked) {
            mainApp.getCurrencyAppData().add(tempCurrencyApp);
        }
    }

    /**
     * Вызывается, когда пользователь кликает по кнопка Edit...
     * Открывает диалоговое окно для изменения выбраной погоды.
     */

    @FXML
    void handleEditIqairApp() {
        IQAirApp selectedIQAirApp = iqairAppTable.getSelectionModel().getSelectedItem();
        if (selectedIQAirApp != null) {
            boolean okClicked = mainApp.showIqAirAppEditDialog(selectedIQAirApp);
        } else {
            // Ничего не выбрано.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Ничего не выбрано");
            alert.setHeaderText("Не выбрана ни одна IQAir");
            alert.setContentText("Пожалуйста, выберите настройки IQAir в таблице");

            alert.showAndWait();
        }
    }
    @FXML
    private void handleEditWeatherApp() {
        WeatherApp selectedWeatherApp = weatherAppTable.getSelectionModel().getSelectedItem();
        if (selectedWeatherApp != null) {
            boolean okClicked = mainApp.showWeatherAppEditDialog(selectedWeatherApp);
        } else {
            // Ничего не выбрано.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Ничего не выбрано");
            alert.setHeaderText("Не выбрана ни одна погода");
            alert.setContentText("Пожалуйста, выберите настройки погоды в таблице");

            alert.showAndWait();
        }
    }

    /**
     * Вызывается, когда пользователь кликает по кнопка Edit...
     * Открывает диалоговое окно для изменения выбранного адресата.
     */
    @FXML
    private void handleEditTrafficApp() {
        TrafficApp selectedTrafficApp = trafficAppTable.getSelectionModel().getSelectedItem();
        if (selectedTrafficApp != null) {
            boolean okClicked = mainApp.showTrafficAppEditDialog(selectedTrafficApp);

        } else {
            // Ничего не выбрано.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Ничего не выбрано");
            alert.setHeaderText("Не выбрана ни одна настройка пробок");
            alert.setContentText("Пожалуйста, выберите настройки пробок в таблице");

            alert.showAndWait();
        }
    }

    /**
     * Вызывается, когда пользователь кликает по кнопка Edit...
     * Открывает диалоговое окно для изменения выбранного адресата.
     */
    @FXML
    private void handleEditCurrencyApp() {
        CurrencyApp selectedCurrencyApp = currencyAppTable.getSelectionModel().getSelectedItem();
        if (selectedCurrencyApp != null) {
            boolean okClicked = mainApp.showCurrencyAppEditDialog(selectedCurrencyApp);

        } else {
            // Ничего не выбрано.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Ничего не выбрано");
            alert.setHeaderText("Не выбрана ни одна настройка курса");
            alert.setContentText("Пожалуйста, выберите настройки курса в таблице");

            alert.showAndWait();
        }
    }


    // выводим текст в TextArea погоды
    public void setTerminalWeather(String terminalWeather){
        String str = this.terminalWeather.getText();
        str += "\n" + terminalWeather;
        this.terminalWeather.setText(str);
    }

    public void setTerminalTraffic(String terminalTraffic){
        String str = this.terminalTraffic.getText();
        str += "\n" + terminalTraffic;
        this.terminalTraffic.setText(str);
    }

    public void setTerminalCurrency(String terminalCurrency) {
        String str = this.terminalCurrency.getText();
        str += "\n" + terminalCurrency;
        this.terminalCurrency.setText(str);
    }

    public void setTerminalIqair(String terminalIqair) {
        String str = this.terminalIqair.getText();
        str += "\n" + terminalIqair;
        this.terminalIqair.setText(str);
    }
}