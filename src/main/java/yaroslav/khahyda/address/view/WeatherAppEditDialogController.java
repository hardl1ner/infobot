package main.java.yaroslav.khahyda.address.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.java.yaroslav.khahyda.address.model.WeatherApp;

import java.io.File;

/**
 * Окно для изменения информации об погоде.
 *
 * @author Yaroslav Khakhyda
 */
public class WeatherAppEditDialogController {
    @FXML
    private TextField projectNameField;
    @FXML
    private TextField stationNameField;
    @FXML
    private TextField urlField;
    @FXML
    private TextField timeUpdateField;
    @FXML
    private TextField dictionaryField;
    @FXML
    private ComboBox<String> dayCombobox;

    @FXML
    private TextField idTodayField;
    @FXML
    private TextField idTodayImgField;
    @FXML
    private TextField blockTodayFormatField;

    @FXML
    private TextField idTomorrowField;
    @FXML
    private TextField idTomorrowImgField;
    @FXML
    private TextField blockTomorrowFormatField;

    private Stage dialogWeatherStage;
    private WeatherApp weatherApp;
    private boolean okClicked = false;

    /**
     * Инициализирует класс-контроллер. Этот метод вызывается автоматически
     * после того, как fxml-файл будет загружен.
     */
    @FXML
    private void initialize() {
        ObservableList<String> langs = FXCollections.observableArrayList("today", "tomorrow");
        dayCombobox.setItems(langs);
    }

    /**
     * Устанавливает сцену для этого окна.
     *
     * @param dialogWeatherStage
     */
    public void setDialogWeatherStage(Stage dialogWeatherStage) {
        this.dialogWeatherStage = dialogWeatherStage;
    }

    /**
     * Задаёт погоду, информацию о которой будем менять.
     *
     * @param weatherApp
     */
    public void setWeatherApp(WeatherApp weatherApp) {
        this.weatherApp = weatherApp;

        projectNameField.setText(weatherApp.getProjectName());
        stationNameField.setText(weatherApp.getStationName());
        urlField.setText(weatherApp.getUrl());
        timeUpdateField.setText(Integer.toString(weatherApp.getTimeUpdate()));
        dictionaryField.setText(weatherApp.getDictionary());
        dayCombobox.setValue(weatherApp.getDayUpdate());

        idTodayField.setText(String.valueOf(weatherApp.getIdToday()));
        idTodayImgField.setText(String.valueOf(weatherApp.getIdTodayImg()));
        blockTodayFormatField.setText(weatherApp.getBlockTodayFormat());

        idTomorrowField.setText(String.valueOf(weatherApp.getIdTomorrow()));
        idTomorrowImgField.setText(String.valueOf(weatherApp.getIdTomorrowImg()));
        blockTomorrowFormatField.setText(weatherApp.getBlockTomorrowFormat());
    }
    /**
     * Returns true, если пользователь кликнул OK, в другом случае false.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Вызывается, когда пользователь кликнул по кнопке OK.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            weatherApp.setProjectName(projectNameField.getText());
            weatherApp.setStationName(stationNameField.getText());
            weatherApp.setUrl(urlField.getText());
            weatherApp.setTimeUpdate(Integer.parseInt(timeUpdateField.getText()));
            weatherApp.setDictionary(dictionaryField.getText());
            weatherApp.setDayUpdate(dayCombobox.getValue());

            weatherApp.setIdToday(Long.parseLong(idTodayField.getText()));
            weatherApp.setIdTodayImg(Long.parseLong(idTodayImgField.getText()));
            weatherApp.setBlockTodayFormat(blockTodayFormatField.getText());

            weatherApp.setIdTomorrow(Long.parseLong(idTomorrowField.getText()));
            weatherApp.setIdTomorrowImg(Long.parseLong(idTomorrowImgField.getText()));
            weatherApp.setBlockTomorrowFormat(blockTomorrowFormatField.getText());

            okClicked = true;
            dialogWeatherStage.close();
        }
    }
    /**
     * Вызывается, когда пользователь кликнул по кнопке Cancel.
     */
    @FXML
    private void handleCancel() {
        dialogWeatherStage.close();
    }

    /**
     * Проверяет пользовательский ввод в текстовых полях.
     *
     * @return true, если пользовательский ввод корректен
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (projectNameField.getText() == null || projectNameField.getText().length() == 0) {
            errorMessage += "Нет введенного имени проэкта!\n";
        }
        if (stationNameField.getText() == null || stationNameField.getText().length() == 0) {
            errorMessage += "Нет введенного имени станции!\n";
        }

        if (urlField.getText() == null || urlField.getText().length() == 0) {
            errorMessage += "Не введен сайт!\n";
        }

        if (timeUpdateField.getText() == null || timeUpdateField.getText().length() == 0) {
            errorMessage += "Не введено время обновления!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Integer.parseInt(timeUpdateField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное время (должно быть целое число)!\n";
            }
        }

        if (dictionaryField.getText() == null || dictionaryField.getText().length() == 0) {
            errorMessage += "Не введено имя словаря!\n";
        }

        if (dayCombobox.getValue() == null || dayCombobox.getValue().length() == 0) {
            errorMessage += "Не введен день обновления!\n";
        }

        if (idTodayField.getText() == null || idTodayField.getText().length() == 0) {
            errorMessage += "Не введен блок погоды на сегодня!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Integer.parseInt(idTodayField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id today (должно быть целое число)!\n";
            }
        }

        if (idTodayImgField.getText() == null || idTodayImgField.getText().length() == 0) {
            errorMessage += "Не введен блок погоды картинки на сегодня!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Integer.parseInt(idTodayImgField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id today img (должно быть целое число)!\n";
            }
        }

        if (blockTodayFormatField.getText() == null || blockTodayFormatField.getText().length() == 0) {
            errorMessage += "Не введен формат для погоды на сегодня!\n";
        }

        if (idTomorrowField.getText() == null || idTomorrowField.getText().length() == 0) {
            errorMessage += "Не введен блок погоды на завтра!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Integer.parseInt(idTomorrowField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id tomorrow (должно быть целое число)!\n";
            }
        }

        if (idTomorrowImgField.getText() == null || idTomorrowImgField.getText().length() == 0) {
            errorMessage += "Не введен блок погоды картинки на завтра!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Integer.parseInt(idTomorrowImgField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное id tomorrow img (должно быть целое число)!\n";
            }
        }

        if (blockTomorrowFormatField.getText() == null || blockTomorrowFormatField.getText().length() == 0) {
            errorMessage += "Не введен формат для погоды на завтра!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Показываем сообщение об ошибке.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogWeatherStage);
            alert.setTitle("Неверные поля");
            alert.setHeaderText("Пожалуйста, заполните правильно поля");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    //открытие файла и возвращения его пути в textfield
    @FXML
    private void handleBrowserDictionary() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Open File txt");
        // Задаём фильтр расширений
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "TXT files (*.txt)", "*.txt");
        chooser.getExtensionFilters().add(extFilter);
        File file = chooser.showOpenDialog(new Stage());
        dictionaryField.setText(file.getAbsolutePath());
    }

}
