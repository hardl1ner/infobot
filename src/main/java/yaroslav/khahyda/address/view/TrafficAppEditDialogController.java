package main.java.yaroslav.khahyda.address.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import main.java.yaroslav.khahyda.address.model.TrafficApp;

import java.io.File;

/**
 * Окно для изменения информации об пробках.
 *
 * @author Yaroslav Khakhyda
 */
public class TrafficAppEditDialogController {
    @FXML
    private TextField trafficProjectNameField;
    @FXML
    private TextField trafficStationNameField;
    @FXML
    private TextField trafficFilePatchField;
    @FXML
    private TextField trafficUrlField;
    @FXML
    private TextField trafficTimeUpdateField;
    @FXML
    private TextField trafficBlockImgField;
    @FXML
    private TextField trafficDictionaryField;

    private Stage dialogTrafficStage;
    private TrafficApp trafficApp;
    private boolean okClicked = false;

    /**
     * Инициализирует класс-контроллер. Этот метод вызывается автоматически
     * после того, как fxml-файл будет загружен.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Устанавливает сцену для этого окна.
     *
     * @param dialogTrafficStage
     */
    public void setDialogTrafficStage(Stage dialogTrafficStage) {
        this.dialogTrafficStage = dialogTrafficStage;
    }

    /**
     * Задаёт погоду, информацию о которой будем менять.
     *
     * @param trafficApp
     */
    public void setTrafficApp(TrafficApp trafficApp) {
        this.trafficApp = trafficApp;

        trafficProjectNameField.setText(trafficApp.getProjectName());
        trafficStationNameField.setText(trafficApp.getStationName());
        trafficFilePatchField.setText(trafficApp.getFilePath());
        trafficUrlField.setText(trafficApp.getUrl());
        trafficTimeUpdateField.setText(Integer.toString(trafficApp.getTimeUpdate()));
        trafficBlockImgField.setText(trafficApp.getBlockTrafficImg());
        trafficDictionaryField.setText(trafficApp.getDictionary());
    }

    /**
     * Returns true, если пользователь кликнул OK, в другом случае false.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }


    /**
     * Вызывается, когда пользователь кликнул по кнопке OK.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            trafficApp.setProjectName(trafficProjectNameField.getText());
            trafficApp.setStationName(trafficStationNameField.getText());
            trafficApp.setFilePath(trafficFilePatchField.getText());
            trafficApp.setUrl(trafficUrlField.getText());
            trafficApp.setTimeUpdate(Integer.parseInt(trafficTimeUpdateField.getText()));
            trafficApp.setBlockTrafficImg(trafficBlockImgField.getText());
            trafficApp.setDictionary(trafficDictionaryField.getText());

            okClicked = true;
            dialogTrafficStage.close();
        }
    }
    /**
     * Вызывается, когда пользователь кликнул по кнопке Cancel.
     */
    @FXML
    private void handleCancel() {
        dialogTrafficStage.close();
    }

    /**
     * Проверяет пользовательский ввод в текстовых полях.
     *
     * @return true, если пользовательский ввод корректен
     */
    private boolean isInputValid() {
        String errorMessage = "";

        if (trafficProjectNameField.getText() == null || trafficProjectNameField.getText().length() == 0) {
            errorMessage += "Нет введенного имени проэкта!!\n";
        }
        if (trafficStationNameField.getText() == null || trafficStationNameField.getText().length() == 0) {
            errorMessage += "Нет введенного имени станции!!\n";
        }
        if (trafficFilePatchField.getText() == null || trafficFilePatchField.getText().length() == 0) {
            errorMessage += "Не выбран или не указан путь к файлу .xml!!\n";
        }
        if (trafficUrlField.getText() == null || trafficUrlField.getText().length() == 0) {
            errorMessage += "Не введен сайт!!\n";
        }

        if (trafficTimeUpdateField.getText() == null || trafficTimeUpdateField.getText().length() == 0) {
            errorMessage += "Не введено время обновления!!\n";
        } else {
            // пытаемся преобразовать время в int.
            try {
                Integer.parseInt(trafficTimeUpdateField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Не действительное время (должно быть целое число)!\n";
            }
        }

        if (trafficBlockImgField.getText() == null || trafficBlockImgField.getText().length() == 0) {
            errorMessage += "Не введен блок картинки пробок!\n";
        }

        if (trafficDictionaryField.getText() == null || trafficDictionaryField.getText().length() == 0) {
            errorMessage += "Не введено имя словаря!\n";
        }


        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Показываем сообщение об ошибке.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogTrafficStage);
            alert.setTitle("Неверные поля пробок");
            alert.setHeaderText("Пожалуйста, заполните правильно поля");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    //открытие файла и возвращения его пути в textfield
    @FXML
    private void handleBrowser() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Open File xml");
        // Задаём фильтр расширений
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML files (*.xml)", "*.xml");
        chooser.getExtensionFilters().add(extFilter);
        File file = chooser.showOpenDialog(new Stage());
        trafficFilePatchField.setText(file.getAbsolutePath());
    }

    //открытие файла и возвращения его пути в textfield
    @FXML
    private void handleBrowserDictionary() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Open File txt");
        // Задаём фильтр расширений
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "TXT files (*.txt)", "*.txt");
        chooser.getExtensionFilters().add(extFilter);
        File file = chooser.showOpenDialog(new Stage());
        trafficDictionaryField.setText(file.getAbsolutePath());
    }

}
